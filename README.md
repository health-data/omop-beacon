# Omop Beacon

This project is an attempt to align Beacon with OMOP data model (stored in Postgres database)

## Related projects

- https://ohdsi.github.io/CommonDataModel/cdm54.html
- https://github.com/ga4gh-beacon/beacon-v2/

- https://github.com/elixir-luxembourg/BH2021-beacon-2.x-omop
- https://github.com/phenopackets/OMOP2Phenopacket
- https://github.com/mrueda/convert-pheno
