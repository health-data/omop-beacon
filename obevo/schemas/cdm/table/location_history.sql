//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0"
CREATE TABLE IF NOT EXISTS cdm.location_history
(
    location_history_id bigint NOT NULL,
    location_id bigint NOT NULL,
    entity_id bigint NOT NULL,
    relationship_type_concept_id integer NOT NULL,
    start_date date NOT NULL,
    end_date date,
    domain_id text NOT NULL
      CONSTRAINT chk_location_history_domain_id CHECK ( length(domain_id) <= 50 ),
    CONSTRAINT xpk_location_history_id PRIMARY KEY (location_history_id),
    CONSTRAINT fpk_location_history_relationship_type_concept_id FOREIGN KEY (relationship_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.location_history
    IS 'The LOCATION HISTORY table stores relationships between Persons or Care Sites and geographic locations over time.';

COMMENT ON COLUMN cdm.location_history.location_history_id
    IS 'A unique identifier for each location history record (PK).';

COMMENT ON COLUMN cdm.location_history.location_id
    IS 'A foreign key to the location table.';

COMMENT ON COLUMN cdm.location_history.relationship_type_concept_id
    IS 'The type of relationship between location and entity.';

COMMENT ON COLUMN cdm.location_history.domain_id
    IS 'The domain of the entity that is related to the location. Either PERSON, PROVIDER, or CARE_SITE.';

COMMENT ON COLUMN cdm.location_history.entity_id
    IS 'The unique identifier for the entity. References either person_id, provider_id, or care_site_id, depending on domain_id.';

COMMENT ON COLUMN cdm.location_history.start_date
    IS 'The date the relationship started.';

COMMENT ON COLUMN cdm.location_history.end_date
    IS 'The date the relationship ended.';

GO

