//// CHANGE name=change0 dependencies="note.change0"
create table cdm.note_nlp
(
    note_nlp_id                bigserial
        constraint xpk_note_nlp
            primary key,
    note_id                    bigint                                                          not null
        constraint fpk_note_nlp_note
            references cdm.note
            deferrable,
    section_concept_id         integer                                                         not null
        constraint fpk_note_nlp_section_concept
            references vocabularies.concept
            on update cascade
            deferrable,
    snippet                    text,
    "offset"                   text,
    lexical_variant            text                                                            not null,
    note_nlp_concept_id        integer                                                         not null
        constraint fpk_note_nlp_concept
            references vocabularies.concept
            on update cascade
            deferrable,
    nlp_system                 text,
    nlp_date                   date                                                            not null,
    nlp_datetime               timestamp,
    term_exists                text,
    term_temporal              text,
    term_modifiers             text,
    note_nlp_source_concept_id integer                                                         not null
        constraint fpk_note_nlp_concept_s
            references vocabularies.concept
            on update cascade
            deferrable
);

GO

//// CHANGE name=change1 dependencies=change0
comment on table cdm.note_nlp is 'The NOTE_NLP table will encode all output of NLP on clinical notes. Each row represents a single extracted term from a note.';

comment on column cdm.note_nlp.note_nlp_id is 'A unique identifier for each term extracted from a note.';

comment on column cdm.note_nlp.note_id is 'A foreign key to the Note table note the term was';

comment on column cdm.note_nlp.section_concept_id is 'A foreign key to the predefined Concept in the Standardized Vocabularies representing the section of the extracted term.';

comment on column cdm.note_nlp.snippet is 'A small window of text surrounding the term.';

comment on column cdm.note_nlp."offset" is 'Character offset of the extracted term in the input note.';

comment on column cdm.note_nlp.lexical_variant is 'Raw text extracted from the NLP tool.';

comment on column cdm.note_nlp.note_nlp_concept_id is 'A foreign key to the predefined Concept in the Standardized Vocabularies reflecting the normalized concept for the extracted term. Domain of the term is represented as part of the Concept table.';

comment on column cdm.note_nlp.nlp_system is 'Name and version of the NLP system that extracted the term.Useful for data provenance.';

comment on column cdm.note_nlp.nlp_date is 'The date of the note processing.Useful for data provenance.';

comment on column cdm.note_nlp.nlp_datetime is 'The date and time of the note processing. Useful for data provenance.';

comment on column cdm.note_nlp.term_exists is 'A summary modifier that signifies presence or absence of the term for a given patient. Useful for quick querying.';

comment on column cdm.note_nlp.term_temporal is 'An optional time modifier associated with the extracted term. (for now “past” or “present” only). Standardize it later.';

comment on column cdm.note_nlp.term_modifiers is 'A compact description of all the modifiers of the specific term extracted by the NLP system. (e.g. “son has rash” ? “negated=no,subject=family, certainty=undef,conditional=false,general=false”).';

comment on column cdm.note_nlp.note_nlp_source_concept_id is 'A foreign key to a Concept that refers to the code in the source vocabulary used by the NLP system';

GO

//// CHANGE name=change2
create index idx_note_nlp_note_id
    on cdm.note_nlp (note_id);

GO

//// CHANGE name=change3
create index idx_note_nlp_concept_id
    on cdm.note_nlp (note_nlp_concept_id);

GO
