//// CHANGE name=change0 dependencies="public.00_migration.change0,cohort_definition.change0"
CREATE TABLE IF NOT EXISTS cdm.cohort
(
    cohort_definition_id bigint NOT NULL,
    subject_id bigint NOT NULL,
    cohort_start_date date NOT NULL,
    cohort_end_date date NOT NULL,
    CONSTRAINT xpk_cohort_id PRIMARY KEY (cohort_definition_id, subject_id, cohort_start_date, cohort_end_date),
    CONSTRAINT fpk_cohort_cohort_definition_id FOREIGN KEY (cohort_definition_id)
        REFERENCES cdm.cohort_definition (cohort_definition_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.cohort
    IS 'The COHORT table contains records of subjects that satisfy a given set of criteria for a duration of time. The definition of the cohort is contained within the COHORT_DEFINITION table. Cohorts can be constructed of patients (Persons), Providers or Visits.';

COMMENT ON COLUMN cdm.cohort.cohort_definition_id
    IS 'A foreign key to a record in the COHORT_DEFINITION table containing relevant Cohort Definition information.';

COMMENT ON COLUMN cdm.cohort.subject_id
    IS 'A foreign key to the subject in the cohort. These could be referring to records in the PERSON, PROVIDER, VISIT_OCCURRENCE table.';

COMMENT ON COLUMN cdm.cohort.cohort_start_date
    IS 'The date when the Cohort Definition criteria for the Person, Provider or Visit first match.';

COMMENT ON COLUMN cdm.cohort.cohort_end_date
    IS 'The date when the Cohort Definition criteria for the Person, Provider or Visit no longer match or the Cohort membership was terminated.';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_cohort_cohort_definition_id
    ON cdm.cohort USING btree
    (cohort_definition_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_cohort_subject_id
    ON cdm.cohort USING btree
    (subject_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO
