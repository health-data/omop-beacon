//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0,provider.change0,visit_detail.change0,visit_occurrence.change0"
CREATE TABLE IF NOT EXISTS cdm.observation
(
    observation_id bigint NOT NULL,
    person_id bigint NOT NULL,
    observation_datetime timestamp without time zone NOT NULL,
    provider_id bigint,
    visit_occurrence_id bigint,
    visit_detail_id bigint,
    observation_event_id bigint,
    observation_date date,
    observation_concept_id integer NOT NULL,
    observation_type_concept_id integer NOT NULL,
    qualifier_concept_id integer NOT NULL,
    observation_source_concept_id integer NOT NULL,
    obs_event_field_concept_id integer NOT NULL,
    value_as_concept_id integer,
    unit_concept_id integer,
    value_as_number numeric,
    value_as_string text COLLATE pg_catalog."default",
    observation_source_value text COLLATE pg_catalog."default",
    unit_source_value text COLLATE pg_catalog."default",
    qualifier_source_value text COLLATE pg_catalog."default",
    value_source_value text COLLATE pg_catalog."default",
    CONSTRAINT xpk_observation_id PRIMARY KEY (observation_id),
    CONSTRAINT fpk_observation_obs_event_field_concept_id FOREIGN KEY (obs_event_field_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_observation_observation_concept_id FOREIGN KEY (observation_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_observation_observation_source_concept_id FOREIGN KEY (observation_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_observation_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_observation_provider_id FOREIGN KEY (provider_id)
        REFERENCES cdm.provider (provider_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_observation_qualifier_concept_id FOREIGN KEY (qualifier_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_observation_observation_type_concept_id FOREIGN KEY (observation_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_observation_unit_concept_id FOREIGN KEY (unit_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_observation_visit_detail_id FOREIGN KEY (visit_detail_id)
        REFERENCES cdm.visit_detail (visit_detail_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_observation_value_as_concept_id FOREIGN KEY (value_as_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_observation_visit_occurrence_id FOREIGN KEY (visit_occurrence_id)
        REFERENCES cdm.visit_occurrence (visit_occurrence_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.observation
    IS 'The OBSERVATION table captures clinical facts about a Person obtained in the context of examination, questioning or a procedure. Any data that cannot be represented by any other domains, such as social and lifestyle facts, medical history, family history, etc. are recorded here.';

COMMENT ON COLUMN cdm.observation.observation_id
    IS 'A unique identifier for each observation.';

COMMENT ON COLUMN cdm.observation.person_id
    IS 'A foreign key identifier to the Person about whom the observation was recorded. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.observation.observation_concept_id
    IS 'A foreign key to the standard observation concept identifier in the Standardized Vocabularies.';

COMMENT ON COLUMN cdm.observation.observation_date
    IS 'The date of the observation.';

COMMENT ON COLUMN cdm.observation.observation_datetime
    IS 'The date and time of the observation.';

COMMENT ON COLUMN cdm.observation.observation_type_concept_id
    IS 'A foreign key to the predefined concept identifier in the Standardized Vocabularies reflecting the type of the observation.';

COMMENT ON COLUMN cdm.observation.value_as_number
    IS 'The observation result stored as a number. This is applicable to observations where the result is expressed as a numeric value.';

COMMENT ON COLUMN cdm.observation.value_as_string
    IS 'The observation result stored as a string. This is applicable to observations where the result is expressed as verbatim text.';

COMMENT ON COLUMN cdm.observation.value_as_concept_id
    IS 'A foreign key to an observation result stored as a Concept ID. This is applicable to observations where the result can be expressed as a Standard Concept from the Standardized Vocabularies (e.g., positive/negative, present/absent, low/high, etc.).';

COMMENT ON COLUMN cdm.observation.qualifier_concept_id
    IS 'A foreign key to a Standard Concept ID for a qualifier (e.g., severity of drug-drug interaction alert)';

COMMENT ON COLUMN cdm.observation.unit_concept_id
    IS 'A foreign key to a Standard Concept ID of measurement units in the Standardized Vocabularies.';

COMMENT ON COLUMN cdm.observation.provider_id
    IS 'A foreign key to the provider in the PROVIDER table who was responsible for making the observation.';

COMMENT ON COLUMN cdm.observation.visit_occurrence_id
    IS 'A foreign key to the visit in the VISIT_OCCURRENCE table during which the observation was recorded.';

COMMENT ON COLUMN cdm.observation.visit_detail_id
    IS 'A foreign key to the visit in the VISIT_DETAIL table during which the observation was recorded.';

COMMENT ON COLUMN cdm.observation.observation_source_value
    IS 'The observation code as it appears in the source data. This code is mapped to a Standard Concept in the Standardized Vocabularies and the original code is, stored here for reference.';

COMMENT ON COLUMN cdm.observation.observation_source_concept_id
    IS 'A foreign key to a Concept that refers to the code used in the source.';

COMMENT ON COLUMN cdm.observation.unit_source_value
    IS 'The source code for the unit as it appears in the source data. This code is mapped to a standard unit concept in the Standardized Vocabularies and the original code is, stored here for reference.';

COMMENT ON COLUMN cdm.observation.qualifier_source_value
    IS 'The source value associated with a qualifier to characterize the observation';

COMMENT ON COLUMN cdm.observation.observation_event_id
    IS 'A foreign key to an event table (e.g., PROCEDURE_OCCURRENCE_ID).';

COMMENT ON COLUMN cdm.observation.obs_event_field_concept_id
    IS 'A foreign key that refers to a Standard Concept identifier in the Standardized Vocabularies referring to the field represented in the OBSERVATION_EVENT_ID.';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_observation_observation_concept_id
    ON cdm.observation USING btree
    (observation_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_observation_person_id
    ON cdm.observation USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change4
CREATE INDEX IF NOT EXISTS idx_observation_visit_occurrence_id
    ON cdm.observation USING btree
    (visit_occurrence_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change5
CREATE INDEX IF NOT EXISTS idx_observation_obs_event_field_concept_id
    ON cdm.observation USING btree
    (obs_event_field_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change6
CREATE INDEX IF NOT EXISTS idx_observation_observation_concept_id
    ON cdm.observation USING btree
    (observation_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO


//// CHANGE name=change7
CREATE INDEX IF NOT EXISTS idx_observation_qualifier_concept_id
    ON cdm.observation USING btree
    (qualifier_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change8
CREATE INDEX IF NOT EXISTS idx_observation_observation_type_concept_id
    ON cdm.observation USING btree
    (observation_type_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change9
CREATE INDEX IF NOT EXISTS idx_observation_unit_concept_id
    ON cdm.observation USING btree
    (unit_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change10
CREATE INDEX IF NOT EXISTS idx_observation_value_as_concept_id
    ON cdm.observation USING btree
    (value_as_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change11
create index if not exists idx_observation_observation_datetime on cdm.observation(observation_datetime);
GO

//// CHANGE name=change12
CREATE INDEX IF NOT EXISTS idx_observation_visit_detail_id ON cdm.observation(visit_detail_id);
GO

-- //// CHANGE name=change13
-- DROP INDEX IF EXISTS cdm.idx_observation_obs_event_field_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_observation_observation_type_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_observation_qualifier_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_observation_unit_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_observation_value_as_concept_id;

-- GO
