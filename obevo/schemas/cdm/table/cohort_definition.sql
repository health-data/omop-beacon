//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0"
CREATE TABLE IF NOT EXISTS cdm.cohort_definition
(
    cohort_definition_id integer,
    definition_type_concept_id integer NOT NULL,
    subject_concept_id integer NOT NULL,
    cohort_initiation_date date,
    cohort_definition_name text NOT NULL
      CONSTRAINT chk_cohort_definition_cohort_definition_name CHECK (length(cohort_definition_name) <= 255),
    cohort_definition_description text,
    cohort_definition_syntax text,
    CONSTRAINT xpk_cohort_definition_id PRIMARY KEY (cohort_definition_id),
    CONSTRAINT fpk_cohort_definition_definition_type_concept_id FOREIGN KEY (definition_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_cohort_definition_subject_concept_id FOREIGN KEY (subject_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.cohort_definition
    IS 'The COHORT_DEFINITION table contains records defining a Cohort derived from the data through the associated description and syntax and upon instantiation (execution of the algorithm) placed into the COHORT table.';

COMMENT ON COLUMN cdm.cohort_definition.cohort_definition_id
    IS 'This is the identifier given to the cohort, usually by the ATLAS application.';

COMMENT ON COLUMN cdm.cohort_definition.cohort_definition_name
    IS 'A short description of the cohort.';

COMMENT ON COLUMN cdm.cohort_definition.cohort_definition_description
    IS 'A complete description of the cohort.';

COMMENT ON COLUMN cdm.cohort_definition.definition_type_concept_id
    IS 'Type defining what kind of Cohort Definition the record represents and how the syntax may be executed.';

COMMENT ON COLUMN cdm.cohort_definition.cohort_definition_syntax
    IS 'Syntax or code to operationalize the Cohort Definition.';

COMMENT ON COLUMN cdm.cohort_definition.subject_concept_id
    IS 'This field contains a Concept that represents the domain of the subjects that are members of the cohort (e.g., Person, Provider, Visit).';

COMMENT ON COLUMN cdm.cohort_definition.cohort_initiation_date
    IS 'A date to indicate when the Cohort was initiated in the COHORT table.';

GO