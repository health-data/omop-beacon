//// CHANGE name=change0 dependencies="public.00_migration.change0"
CREATE TABLE IF NOT EXISTS cdm.episode
(
    episode_id bigint NOT NULL,
    person_id bigint NOT NULL,
    episode_parent_id bigint,
    episode_start_datetime timestamp without time zone,
    episode_end_datetime timestamp without time zone,
    episode_start_date date NOT NULL,
    episode_end_date date,
    episode_concept_id integer NOT NULL,
    episode_number integer,
    episode_object_concept_id integer NOT NULL,
    episode_type_concept_id integer NOT NULL,
    episode_source_concept_id integer,
    episode_source_value text
      CONSTRAINT chk_episode_episode_source_value CHECK ( coalesce(length(episode_source_value), 0) <= 50 )
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1
create index if not exists idx_episode_episode_start_datetime on cdm.episode(episode_start_datetime);
GO
