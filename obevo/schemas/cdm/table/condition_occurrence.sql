//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0,provider.change0,visit_occurrence.change0,visit_detail.change0"
CREATE TABLE IF NOT EXISTS cdm.condition_occurrence
(
    condition_occurrence_id bigint,
    person_id bigint,
    provider_id bigint,
    visit_occurrence_id bigint,
    visit_detail_id bigint,
    condition_start_datetime timestamp without time zone,
    condition_end_datetime timestamp without time zone,
    condition_concept_id integer,
    condition_type_concept_id integer,
    condition_status_concept_id integer,
    condition_source_concept_id integer,
    condition_start_date date,
    condition_end_date date,
    stop_reason text
      CONSTRAINT chk_condition_occurrence_stop_reason CHECK( coalesce(length(stop_reason), 0) <= 20),
    condition_source_value text,
    condition_status_source_value text,
    CONSTRAINT xpk_condition_occurrence_id PRIMARY KEY (condition_occurrence_id),
    CONSTRAINT fpk_condition_occurrence_person_id FOREIGN KEY (person_id)
      REFERENCES cdm.person (person_id)
      ON UPDATE CASCADE ON DELETE NO ACTION,
    CONSTRAINT fpk_condition_occurrence_provider_id FOREIGN KEY (provider_id)
      REFERENCES cdm.provider (provider_id) ON UPDATE CASCADE,
    CONSTRAINT fpk_condition_occurrence_visit_occurrence_id FOREIGN KEY (visit_occurrence_id)
      REFERENCES cdm.visit_occurrence(visit_occurrence_id) ON UPDATE CASCADE,
    CONSTRAINT fpk_condition_occurrence_visit_detail_id FOREIGN KEY (visit_detail_id)
      REFERENCES cdm.visit_detail(visit_detail_id) ON UPDATE CASCADE,
    CONSTRAINT fpk_condition_occurrence_condition_concept_id FOREIGN KEY (condition_concept_id)
      REFERENCES vocabularies.concept(concept_id)
      ON UPDATE CASCADE ON DELETE NO ACTION DEFERRABLE,
    CONSTRAINT fpk_condition_occurrence_condition_type_concept_id FOREIGN KEY (condition_type_concept_id)
      REFERENCES vocabularies.concept(concept_id)
      ON UPDATE CASCADE ON DELETE NO ACTION DEFERRABLE,
    CONSTRAINT fpk_condition_occurrence_condition_status_concept_id FOREIGN KEY (condition_status_concept_id)
      REFERENCES vocabularies.concept(concept_id)
      ON UPDATE CASCADE ON DELETE NO ACTION DEFERRABLE,
    CONSTRAINT fpk_condition_occurrence_condition_source_concept_id FOREIGN KEY (condition_source_concept_id)
      REFERENCES vocabularies.concept(concept_id)
      ON UPDATE CASCADE ON DELETE NO ACTION DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.condition_occurrence
    IS 'Conditions are records of a Person suggesting the presence of a disease or medical condition stated as a diagnosis, a sign, or a symptom, which is either observed by a Provider or reported by the patient. Conditions are recorded in different sources and levels of standardization, for example:  * Medical claims data include diagnoses coded in Source Vocabularies such as ICD-9-CM that are submitted as part of a reimbursement claim for health services * EHRs may capture Person conditions in the form of diagnosis codes or symptoms';

COMMENT ON COLUMN cdm.condition_occurrence.condition_occurrence_id
    IS 'A unique identifier for each Condition Occurrence event.';

COMMENT ON COLUMN cdm.condition_occurrence.person_id
    IS 'A foreign key identifier to the Person who is experiencing the condition. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.condition_occurrence.condition_concept_id
    IS 'A foreign key that refers to a Standard Concept identifier in the Standardized Vocabularies belonging to the ''Condition'' domain.';

COMMENT ON COLUMN cdm.condition_occurrence.condition_start_date
    IS 'The date when the instance of the Condition is recorded.';

COMMENT ON COLUMN cdm.condition_occurrence.condition_start_datetime
    IS 'The date and time when the instance of the Condition is recorded.';

COMMENT ON COLUMN cdm.condition_occurrence.condition_end_date
    IS 'The date when the instance of the Condition is considered to have ended.';

COMMENT ON COLUMN cdm.condition_occurrence.condition_end_datetime
    IS 'The date when the instance of the Condition is considered to have ended.';

COMMENT ON COLUMN cdm.condition_occurrence.condition_type_concept_id
    IS 'A foreign key to the predefined Concept identifier in the Standardized Vocabularies reflecting the source data from which the Condition was recorded, the level of standardization, and the type of occurrence. These belong to the ''Condition Type'' vocabulary';

COMMENT ON COLUMN cdm.condition_occurrence.condition_status_concept_id
    IS 'A foreign key that refers to a Standard Concept identifier in the Standardized Vocabularies reflecting the point of care at which the Condition was diagnosed.';

COMMENT ON COLUMN cdm.condition_occurrence.stop_reason
    IS 'The reason that the Condition was no longer present, as indicated in the source data.';

COMMENT ON COLUMN cdm.condition_occurrence.provider_id
    IS 'A foreign key to the Provider in the PROVIDER table who was responsible for capturing (diagnosing) the Condition.';

COMMENT ON COLUMN cdm.condition_occurrence.visit_occurrence_id
    IS 'A foreign key to the visit in the VISIT_OCCURRENCE table during which the Condition was determined (diagnosed).';

COMMENT ON COLUMN cdm.condition_occurrence.visit_detail_id
    IS 'A foreign key to the visit in the VISIT_DETAIL table during which the Condition was determined (diagnosed).';

COMMENT ON COLUMN cdm.condition_occurrence.condition_source_value
    IS 'The source code for the Condition as it appears in the source data. This code is mapped to a Standard Condition Concept in the Standardized Vocabularies and the original code is stored here for reference.';

COMMENT ON COLUMN cdm.condition_occurrence.condition_source_concept_id
    IS 'A foreign key to a Condition Concept that refers to the code used in the source.';

COMMENT ON COLUMN cdm.condition_occurrence.condition_status_source_value
    IS 'The source code for the condition status as it appears in the source data.  This code is mapped to a Standard Concept in the Standardized Vocabularies and the original code is stored here for reference.';


GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_condition_occurrence_person_id
    ON cdm.condition_occurrence USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_condition_occurrence_condition_concept_id
    ON cdm.condition_occurrence USING btree
    (condition_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change4
CREATE INDEX IF NOT EXISTS idx_condition_occurrence_condition_type_concept_id
    ON cdm.condition_occurrence USING btree
    (condition_type_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change5
CREATE INDEX IF NOT EXISTS idx_condition_occurrence_condition_status_concept_id
    ON cdm.condition_occurrence USING btree
    (condition_status_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change6
CREATE INDEX IF NOT EXISTS idx_condition_occurrence_condition_source_concept_id
    ON cdm.condition_occurrence USING btree
    (condition_source_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change7
ALTER TABLE cdm.condition_occurrence
    ALTER CONSTRAINT fpk_condition_occurrence_person_id DEFERRABLE INITIALLY IMMEDIATE
  , ALTER CONSTRAINT fpk_condition_occurrence_provider_id DEFERRABLE INITIALLY IMMEDIATE
  , ALTER CONSTRAINT fpk_condition_occurrence_visit_occurrence_id DEFERRABLE INITIALLY IMMEDIATE
  , ALTER CONSTRAINT fpk_condition_occurrence_visit_detail_id DEFERRABLE INITIALLY IMMEDIATE
;

GO

//// CHANGE name=change8
create index if not exists idx_condition_occurrence_condition_start_datetime on cdm.condition_occurrence(condition_start_datetime);
GO

//// CHANGE name=change9
CREATE INDEX IF NOT EXISTS idx_condition_occurrence_visit_detail_id ON cdm.condition_occurrence(visit_detail_id);
CREATE INDEX IF NOT EXISTS idx_condition_occurrence_visit_occurrence_id ON cdm.condition_occurrence(visit_occurrence_id);
GO
