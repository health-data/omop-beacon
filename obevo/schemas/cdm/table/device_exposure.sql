//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0,provider.change0,,visit_detail.change0,visit_occurrence.change0"
CREATE TABLE IF NOT EXISTS cdm.device_exposure
(
    device_exposure_id bigint NOT NULL,
    person_id bigint NOT NULL,
    provider_id bigint,
    visit_occurrence_id bigint,
    visit_detail_id bigint,
    device_exposure_start_datetime timestamp without time zone NOT NULL,
    device_exposure_end_datetime timestamp without time zone,
    device_concept_id integer NOT NULL,
    device_type_concept_id integer NOT NULL,
    quantity integer,
    device_source_concept_id integer NOT NULL,
    unit_concept_id integer,
    unit_source_concept_id integer,
    device_exposure_start_date date,
    device_exposure_end_date date,
    unique_device_id text COLLATE pg_catalog."default",
    device_source_value text COLLATE pg_catalog."default",
    production_id text COLLATE pg_catalog."default",
    unit_source_value text COLLATE pg_catalog."default",
    CONSTRAINT xpk_device_exposure_id PRIMARY KEY (device_exposure_id),
    CONSTRAINT fpk_device_exposure_device_concept_id FOREIGN KEY (device_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_device_exposure_device_source_concept_id FOREIGN KEY (device_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_device_exposure_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_device_exposure_provider_id FOREIGN KEY (provider_id)
        REFERENCES cdm.provider (provider_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_device_exposure_device_type_concept_id FOREIGN KEY (device_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_device_exposure_visit_detail_id FOREIGN KEY (visit_detail_id)
        REFERENCES cdm.visit_detail (visit_detail_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_device_exposure_visit_occurrence_id FOREIGN KEY (visit_occurrence_id)
        REFERENCES cdm.visit_occurrence (visit_occurrence_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.device_exposure
    IS 'The ''Device'' domain captures information about a person''s exposure to a foreign physical object or instrument which is used for diagnostic or therapeutic purposes through a mechanism beyond chemical action. Devices include implantable objects (e.g. pacemakers, stents, artificial joints), medical equipment and supplies (e.g. bandages, crutches, syringes), other instruments used in medical procedures (e.g. sutures, defibrillators) and material used in clinical care (e.g. adhesives, body material, dental material, surgical material).';

COMMENT ON COLUMN cdm.device_exposure.device_exposure_id
    IS 'A system-generated unique identifier for each Device Exposure.';

COMMENT ON COLUMN cdm.device_exposure.person_id
    IS 'A foreign key identifier to the Person who is subjected to the Device. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.device_exposure.device_concept_id
    IS 'A foreign key that refers to a Standard Concept identifier in the Standardized Vocabularies belonging to the ''Device'' domain.';

COMMENT ON COLUMN cdm.device_exposure.device_exposure_start_date
    IS 'The date the Device or supply was applied or used.';

COMMENT ON COLUMN cdm.device_exposure.device_exposure_start_datetime
    IS 'The date and time the Device or supply was applied or used.';

COMMENT ON COLUMN cdm.device_exposure.device_exposure_end_date
    IS 'The date use of the Device or supply was ceased.';

COMMENT ON COLUMN cdm.device_exposure.device_exposure_end_datetime
    IS 'The date and time use of the Device or supply was ceased.';

COMMENT ON COLUMN cdm.device_exposure.device_type_concept_id
    IS 'A foreign key to the predefined Concept identifier in the Standardized Vocabularies reflecting the type of Device Exposure recorded. It indicates how the Device Exposure was represented in the source data and belongs to the ''Device Type'' domain.';

COMMENT ON COLUMN cdm.device_exposure.unique_device_id
    IS 'A UDI or equivalent identifying the instance of the Device used in the Person.';

COMMENT ON COLUMN cdm.device_exposure.quantity
    IS 'The number of individual Devices used in the exposure.';

COMMENT ON COLUMN cdm.device_exposure.provider_id
    IS 'A foreign key to the provider in the PROVIDER table who initiated or administered the Device.';

COMMENT ON COLUMN cdm.device_exposure.visit_occurrence_id
    IS 'A foreign key to the visit in the VISIT_OCCURRENCE table during which the Device was used.';

COMMENT ON COLUMN cdm.device_exposure.visit_detail_id
    IS 'A foreign key to the visit detail record in the VISIT_DETAIL table during which the Device was used.';

COMMENT ON COLUMN cdm.device_exposure.device_source_value
    IS 'The source code for the Device as it appears in the source data. This code is mapped to a Standard Device Concept in the Standardized Vocabularies and the original code is stored here for reference.';

COMMENT ON COLUMN cdm.device_exposure.device_source_concept_id
    IS 'A foreign key to a Device Concept that refers to the code used in the source.';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_device_exposure_device_concept_id
    ON cdm.device_exposure USING btree
    (device_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_device_exposure_person_id
    ON cdm.device_exposure USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change4
CREATE INDEX IF NOT EXISTS idx_device_exposure_visit_occurrence_id
    ON cdm.device_exposure USING btree
    (visit_occurrence_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change5
CREATE INDEX IF NOT EXISTS idx_device_exposure_visit_detail_id
    ON cdm.device_exposure USING btree
    (visit_detail_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change6
create index if not exists idx_device_exposure_device_exposure_start_datetime on cdm.device_exposure(device_exposure_start_datetime);
GO

