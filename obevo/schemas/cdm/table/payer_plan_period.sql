//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0"
CREATE TABLE IF NOT EXISTS cdm.payer_plan_period
(
    payer_plan_period_id bigint NOT NULL,
    person_id bigint NOT NULL,
    contract_person_id bigint,
    payer_plan_period_start_date date NOT NULL,
    payer_plan_period_end_date date NOT NULL,
    payer_concept_id integer NOT NULL,
    plan_concept_id integer NOT NULL,
    contract_concept_id integer NOT NULL,
    sponsor_concept_id integer NOT NULL,
    stop_reason_concept_id integer NOT NULL,
    payer_source_concept_id integer NOT NULL,
    plan_source_concept_id integer NOT NULL,
    contract_source_concept_id integer NOT NULL,
    sponsor_source_concept_id integer NOT NULL,
    stop_reason_source_concept_id integer NOT NULL,
    payer_source_value text COLLATE pg_catalog."default",
    plan_source_value text COLLATE pg_catalog."default",
    contract_source_value text COLLATE pg_catalog."default",
    sponsor_source_value text COLLATE pg_catalog."default",
    family_source_value text COLLATE pg_catalog."default",
    stop_reason_source_value text COLLATE pg_catalog."default",
    CONSTRAINT xpk_payer_plan_period_id PRIMARY KEY (payer_plan_period_id),
    CONSTRAINT fpk_payer_plan_period_contract_concept_id FOREIGN KEY (contract_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_payer_plan_period_contract_person_id FOREIGN KEY (contract_person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_payer_plan_period_contract_source_concept_id FOREIGN KEY (contract_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_payer_plan_period_payer_concept_id FOREIGN KEY (payer_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_payer_plan_period_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_payer_plan_period_payer_source_concept_id FOREIGN KEY (payer_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_payer_plan_period_plan_concept_id FOREIGN KEY (plan_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_payer_plan_period_plan_source_concept_id FOREIGN KEY (plan_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_payer_plan_period_sponsor_concept_id FOREIGN KEY (sponsor_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_payer_plan_period_sponsor_source_concept_id FOREIGN KEY (sponsor_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_payer_plan_period_stop_reason_concept_id FOREIGN KEY (stop_reason_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_payer_plan_period_stop_reason_source_concept_id FOREIGN KEY (stop_reason_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.payer_plan_period
    IS 'The PAYER_PLAN_PERIOD table captures details of the period of time that a Person is continuously enrolled under a specific health Plan benefit structure from a given Payer. Each Person receiving healthcare is typically covered by a health benefit plan, which pays for (fully or partially), or directly provides, the care. These benefit plans are provided by payers, such as health insurances or state or government agencies. In each plan the details of the health benefits are defined for the Person or her family, and the health benefit Plan might change over time typically with increasing utilization (reaching certain cost thresholds such as deductibles), plan availability and purchasing choices of the Person. The unique combinations of Payer organizations, health benefit Plans and time periods in which they are valid for a Person are recorded in this table.';

COMMENT ON COLUMN cdm.payer_plan_period.payer_plan_period_id
    IS 'A identifier for each unique combination of payer, plan, family code and time span.';

COMMENT ON COLUMN cdm.payer_plan_period.person_id
    IS 'A foreign key identifier to the Person covered by the payer. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.payer_plan_period.contract_person_id
    IS 'A foreign key identifier to the person_id in person table, for the person who is the primary subscriber/contract owner for the record in the payer_plan_period table. Maybe the same person or different person, depending on who is the primary subscriber/contract owner.';

COMMENT ON COLUMN cdm.payer_plan_period.payer_plan_period_start_date
    IS 'The start date of the payer plan period.';

COMMENT ON COLUMN cdm.payer_plan_period.payer_plan_period_end_date
    IS 'The end date of the payer plan period.';

COMMENT ON COLUMN cdm.payer_plan_period.payer_concept_id
    IS 'A foreign key that refers to a standard Payer concept identifier in the Standarized Vocabularies';

COMMENT ON COLUMN cdm.payer_plan_period.plan_concept_id
    IS 'A foreign key that refers to a standard plan concept identifier that represents the health benefit plan in the Standardized Vocabularies.';

COMMENT ON COLUMN cdm.payer_plan_period.contract_concept_id
    IS 'A foreign key to a standard concept representing the reason justifying the contract between person_id and contract_person_id.';

COMMENT ON COLUMN cdm.payer_plan_period.sponsor_concept_id
    IS 'A foreign key that refers to a concept identifier that represents the sponsor in the Standardized Vocabularies.';

COMMENT ON COLUMN cdm.payer_plan_period.stop_reason_concept_id
    IS 'A foreign key that refers to a standard termination reason that represents the reason for the termination in the Standardized Vocabularies.';

COMMENT ON COLUMN cdm.payer_plan_period.payer_source_value
    IS 'The source code for the payer as it appears in the source data.';

COMMENT ON COLUMN cdm.payer_plan_period.payer_source_concept_id
    IS 'A foreign key to a payer concept that refers to the code used in the source.';

COMMENT ON COLUMN cdm.payer_plan_period.plan_source_value
    IS 'The source code for the Person''s health benefit plan as it appears in the source data.';

COMMENT ON COLUMN cdm.payer_plan_period.plan_source_concept_id
    IS 'A foreign key to a plan concept that refers to the plan code used in the source data.';

COMMENT ON COLUMN cdm.payer_plan_period.contract_source_value
    IS 'The source code representing the reason justifying the contract. Usually it is family relationship like a spouse, domestic partner, child etc.';

COMMENT ON COLUMN cdm.payer_plan_period.contract_source_concept_id
    IS 'A foreign key to a concept that refers to the code used in the source as the reason justifying the contract.';

COMMENT ON COLUMN cdm.payer_plan_period.sponsor_source_value
    IS 'The source code for the Person''s sponsor of the health plan as it appears in the source data.';

COMMENT ON COLUMN cdm.payer_plan_period.sponsor_source_concept_id
    IS 'A foreign key to a sponsor concept that refers to the sponsor code used in the source data.';

COMMENT ON COLUMN cdm.payer_plan_period.family_source_value
    IS 'The source code for the Person''s family as it appears in the source data.';

COMMENT ON COLUMN cdm.payer_plan_period.stop_reason_source_value
    IS 'The reason for stop-coverage as it appears in the source data.';

COMMENT ON COLUMN cdm.payer_plan_period.stop_reason_source_concept_id
    IS 'A foreign key to a stop-coverage concept that refers to the code used in the source.';

GO

