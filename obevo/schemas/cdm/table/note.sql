//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0,provider.change0"
CREATE TABLE IF NOT EXISTS cdm.note
(
    note_id bigint NOT NULL,
    person_id bigint NOT NULL,
    note_event_id bigint,
    provider_id bigint,
    visit_occurrence_id bigint,
    visit_detail_id bigint,
    note_datetime timestamp without time zone NOT NULL,
    note_event_field_concept_id integer NOT NULL,
    note_type_concept_id integer NOT NULL,
    note_class_concept_id integer NOT NULL,
    encoding_concept_id integer NOT NULL,
    language_concept_id integer NOT NULL,
    note_date date,
    note_title text
     CONSTRAINT chk_note_note_title CHECK (coalesce(length(note_title), 0) <= 250),
    note_text text COLLATE pg_catalog."default",
    note_source_value text COLLATE pg_catalog."default",
    CONSTRAINT xpk_note_id PRIMARY KEY (note_id),
    CONSTRAINT fpk_note_language_concept_id FOREIGN KEY (language_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_note_note_class_concept_id FOREIGN KEY (note_class_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_note_encoding_concept_id FOREIGN KEY (encoding_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_note_note_event_field_concept_id FOREIGN KEY (note_event_field_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_note_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_note_provider_id FOREIGN KEY (provider_id)
        REFERENCES cdm.provider (provider_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_note_note_type_concept_id FOREIGN KEY (note_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.note
    IS 'The NOTE table captures unstructured information that was recorded by a provider about a patient in free text notes on a given date.';

COMMENT ON COLUMN cdm.note.note_id
    IS 'A unique identifier for each note.';

COMMENT ON COLUMN cdm.note.person_id
    IS 'A foreign key identifier to the Person about whom the Note was recorded. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.note.note_event_id
    IS 'A foreign key identifier to the event (e.g. Measurement, Procedure, Visit, Drug Exposure, etc) record during which the note was recorded.';

COMMENT ON COLUMN cdm.note.note_event_field_concept_id
    IS 'A foreign key to the predefined Concept in the Standardized Vocabularies reflecting the field to which the note_event_id is referring.';

COMMENT ON COLUMN cdm.note.note_date
    IS 'The date the note was recorded.';

COMMENT ON COLUMN cdm.note.note_datetime
    IS 'The date and time the note was recorded.';

COMMENT ON COLUMN cdm.note.note_type_concept_id
    IS 'A foreign key to the predefined Concept in the Standardized Vocabularies reflecting the type, origin or provenance of the Note. These belong to the ''Note Type'' vocabulary';

COMMENT ON COLUMN cdm.note.note_class_concept_id
    IS 'A foreign key to the predefined Concept in the Standardized Vocabularies reflecting the HL7 LOINC Document Type Vocabulary classification of the note.';

COMMENT ON COLUMN cdm.note.note_title
    IS 'The title of the Note as it appears in the source.';

COMMENT ON COLUMN cdm.note.note_text
    IS 'The content of the Note.';

COMMENT ON COLUMN cdm.note.encoding_concept_id
    IS 'A foreign key to the predefined Concept in the Standardized Vocabularies reflecting the note character encoding type';

COMMENT ON COLUMN cdm.note.language_concept_id
    IS 'A foreign key to the predefined Concept in the Standardized Vocabularies reflecting the language of the note';

COMMENT ON COLUMN cdm.note.provider_id
    IS 'A foreign key to the Provider in the PROVIDER table who took the Note.';

COMMENT ON COLUMN cdm.note.visit_occurrence_id
    IS 'A foreign key to the Visit in the VISIT_OCCURRENCE table when the Note was taken.';

COMMENT ON COLUMN cdm.note.visit_detail_id
    IS 'A foreign key to the Visit in the VISIT_DETAIL table when the Note was taken.';

COMMENT ON COLUMN cdm.note.note_source_value
    IS 'The source value associated with the origin of the Note';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_note_note_type_concept_id
    ON cdm.note USING btree
    (note_type_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_note_person_id
    ON cdm.note USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change4
CREATE INDEX IF NOT EXISTS idx_note_visit_occurrence_id
    ON cdm.note USING btree
    (visit_occurrence_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change5
CREATE INDEX IF NOT EXISTS idx_note_note_event_field_concept_id
    ON cdm.note USING btree
    (note_event_field_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change6
CREATE INDEX IF NOT EXISTS idx_note_note_event_id
    ON cdm.note USING btree
    (note_event_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change7
CREATE INDEX IF NOT EXISTS trgm_note_note_text
    ON cdm.note USING gin
    (note_text COLLATE pg_catalog."default" public.gin_trgm_ops)
    TABLESPACE pg_default;
GO

//// CHANGE name=change8
CREATE INDEX IF NOT EXISTS idx_note_visit_detail_id
    ON cdm.note USING btree
    (visit_detail_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change9
CREATE INDEX IF NOT EXISTS idx_note_language_concept_id
    ON cdm.note USING btree
    (language_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change10
CREATE INDEX IF NOT EXISTS idx_note_note_class_concept_id
    ON cdm.note USING btree
    (note_class_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change11
CREATE INDEX IF NOT EXISTS idx_note_encoding_concept_id
    ON cdm.note USING btree
    (encoding_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change12
CREATE INDEX IF NOT EXISTS idx_note_note_event_field_concept_id
    ON cdm.note USING btree
    (note_event_field_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change13
CREATE INDEX IF NOT EXISTS idx_note_note_type_concept_id
    ON cdm.note USING btree
    (note_type_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change14
create index if not exists idx_note_note_datetime on cdm.note(note_datetime);
GO

//// CHANGE name=change15
ALTER TABLE cdm.note
ADD FOREIGN KEY (visit_occurrence_id)
  REFERENCES cdm.visit_occurrence (visit_occurrence_id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE NO ACTION
  DEFERRABLE
, ADD FOREIGN KEY (visit_detail_id)
  REFERENCES cdm.visit_detail (visit_detail_id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE NO ACTION
  DEFERRABLE
;

GO

-- //// CHANGE name=change16
-- DROP INDEX IF EXISTS cdm.idx_note_encoding_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_note_language_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_note_note_class_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_note_note_event_field_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_note_note_type_concept_id;

-- GO