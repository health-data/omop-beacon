//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0"
CREATE TABLE IF NOT EXISTS cdm.observation_period
(
    observation_period_id bigint NOT NULL,
    person_id bigint NOT NULL,
    observation_period_start_date date NOT NULL,
    observation_period_end_date date NOT NULL,
    period_type_concept_id integer NOT NULL,
    CONSTRAINT xpk_observation_period_id PRIMARY KEY (observation_period_id),
    CONSTRAINT fpk_observation_period_period_type_concept_id FOREIGN KEY (period_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_observation_period_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.observation_period
    IS 'The OBSERVATION_PERIOD table contains records which uniquely define the spans of time for which a Person is at-risk to have clinical events recorded within the source systems, even if no events in fact are recorded (healthy patient with no healthcare interactions).';

COMMENT ON COLUMN cdm.observation_period.observation_period_id
    IS 'A unique identifier for each observation period.';

COMMENT ON COLUMN cdm.observation_period.person_id
    IS 'A foreign key identifier to the person for whom the observation period is defined. The demographic details of that person are stored in the person table.';

COMMENT ON COLUMN cdm.observation_period.observation_period_start_date
    IS 'The start date of the observation period for which data are available from the data source.';

COMMENT ON COLUMN cdm.observation_period.observation_period_end_date
    IS 'The end date of the observation period for which data are available from the data source.';

COMMENT ON COLUMN cdm.observation_period.period_type_concept_id
    IS 'A foreign key identifier to the predefined concept in the Standardized Vocabularies reflecting the source of the observation period information, belonging to the ''Obs Period Type'' vocabulary';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_observation_period_person_id
    ON cdm.observation_period USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_observation_period_start_date_person_id
    ON cdm.observation_period USING btree
    (observation_period_start_date ASC NULLS LAST, person_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO
