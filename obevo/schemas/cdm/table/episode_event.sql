//// CHANGE name=change0 dependencies="public.00_migration.change0"
CREATE TABLE IF NOT EXISTS cdm.episode_event
(
    episode_id bigint NOT NULL,
    event_id bigint NOT NULL,
    episode_event_field_concept_id integer NOT NULL
)
TABLESPACE pg_default;

GO

