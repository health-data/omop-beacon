//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0,provider.change0"
CREATE TABLE IF NOT EXISTS cdm.survey_conduct
(
    survey_conduct_id bigint,
    person_id bigint NOT NULL,
    survey_end_datetime timestamp without time zone NOT NULL,
    survey_start_datetime timestamp without time zone,
    visit_occurrence_id bigint,
    visit_detail_id bigint,
    response_visit_occurrence_id bigint,
    provider_id bigint,
    survey_concept_id integer NOT NULL,
    assisted_concept_id integer NOT NULL,
    respondent_type_concept_id integer NOT NULL,
    timing_concept_id integer NOT NULL,
    collection_method_concept_id integer NOT NULL,
    survey_source_concept_id integer NOT NULL,
    validated_survey_concept_id integer NOT NULL,
    survey_start_date date,
    survey_end_date date,
    assisted_source_value text COLLATE pg_catalog."default",
    respondent_type_source_value text COLLATE pg_catalog."default",
    timing_source_value text COLLATE pg_catalog."default",
    collection_method_source_value text COLLATE pg_catalog."default",
    survey_source_value text COLLATE pg_catalog."default",
    survey_source_identifier text
      CONSTRAINT survey_source_identifier_length CHECK ( coalesce(length(survey_source_identifier), 0) <= 100),
    validated_survey_source_value text COLLATE pg_catalog."default",
    survey_version_number text
      CONSTRAINT survey_version_number_length CHECK (coalesce(length(survey_version_number), 0) <= 20),
    CONSTRAINT xpk_survey_conduct_id PRIMARY KEY (survey_conduct_id),
    CONSTRAINT fpk_survey_conduct_collection_method_concept_id FOREIGN KEY (collection_method_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_survey_conduct_respondent_type_concept_id FOREIGN KEY (respondent_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_survey_conduct_assisted_concept_id FOREIGN KEY (assisted_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_survey_conduct_survey_concept_id FOREIGN KEY (survey_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_survey_conduct_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_survey_conduct_provider_id FOREIGN KEY (provider_id)
        REFERENCES cdm.provider (provider_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_survey_conduct_survey_source_concept_id FOREIGN KEY (survey_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_survey_conduct_timing_concept_id FOREIGN KEY (timing_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_survey_conduct_validated_survey_concept_id FOREIGN KEY (validated_survey_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.survey_conduct
    IS 'The SURVEY_CONDUCT table is used to store an instance of a completed survey or questionnaire. It captures details of the individual questionnaire such as who completed it, when it was completed and to which patient treatment or visit it relates to (if any). Each SURVEY has a SURVEY_CONCEPT_ID, a concept in the CONCEPT table identifying the questionnaire e.g. EQ5D, VR12, SF12. Each questionnaire should exist in the CONCEPT table. Each SURVEY can be optionally related to a specific patient visit in order to link it both to the visit during which it was completed and any subsequent visit where treatment was assigned based on the patient''s responses.';

COMMENT ON COLUMN cdm.survey_conduct.survey_conduct_id
    IS 'Unique identifier for each completed survey.';

COMMENT ON COLUMN cdm.survey_conduct.person_id
    IS 'A foreign key identifier to the Person in the PERSON table about whom the survey was completed.';

COMMENT ON COLUMN cdm.survey_conduct.survey_concept_id
    IS 'A foreign key to the predefined Concept identifier in the Standardized Vocabularies reflecting the name and identity of the survey.';

COMMENT ON COLUMN cdm.survey_conduct.survey_start_date
    IS 'Date on which the survey was started.';

COMMENT ON COLUMN cdm.survey_conduct.survey_start_datetime
    IS 'Date and time the survey was started.';

COMMENT ON COLUMN cdm.survey_conduct.survey_end_date
    IS 'Date on which the survey was completed.';

COMMENT ON COLUMN cdm.survey_conduct.survey_end_datetime
    IS 'Date and time the survey was completed.';

COMMENT ON COLUMN cdm.survey_conduct.provider_id
    IS 'A foreign key to the provider in the provider table who was associated with the survey completion.';

COMMENT ON COLUMN cdm.survey_conduct.assisted_concept_id
    IS 'A foreign key to the predefined Concept identifier in the Standardized Vocabularies indicating whether the survey was completed with assistance.';

COMMENT ON COLUMN cdm.survey_conduct.respondent_type_concept_id
    IS 'A foreign key to the predefined Concept identifier in the Standardized Vocabularies reflecting the respondent type. Example: Research Associate, Patient.';

COMMENT ON COLUMN cdm.survey_conduct.timing_concept_id
    IS 'A foreign key to the predefined Concept identifier in the Standardized Vocabularies that refers to a certain timing. Example: 3 month follow-up, 6 month follow-up.';

COMMENT ON COLUMN cdm.survey_conduct.collection_method_concept_id
    IS 'A foreign key to the predefined Concept identifier in the Standardized Vocabularies reflecting the data collection method (e.g. Paper, Telephone, Electronic Questionnaire).';

COMMENT ON COLUMN cdm.survey_conduct.assisted_source_value
    IS 'Source value representing whether patient required assistance to complete the survey. Example: “Completed without assistance”, ”Completed with assistance”.';

COMMENT ON COLUMN cdm.survey_conduct.respondent_type_source_value
    IS 'Source code representing role of person who completed the survey.';

COMMENT ON COLUMN cdm.survey_conduct.timing_source_value
    IS 'Text string representing the timing of the survey. Example: Baseline, 6-month follow-up.';

COMMENT ON COLUMN cdm.survey_conduct.collection_method_source_value
    IS 'The collection method as it appears in the source data.';

COMMENT ON COLUMN cdm.survey_conduct.survey_source_value
    IS 'The survey name/title as it appears in the source data.';

COMMENT ON COLUMN cdm.survey_conduct.survey_source_concept_id
    IS 'A foreign key to a predefined Concept that refers to the code for the survey name/title used in the source.';

COMMENT ON COLUMN cdm.survey_conduct.survey_source_identifier
    IS 'Unique identifier for each completed survey in source system.';

COMMENT ON COLUMN cdm.survey_conduct.validated_survey_concept_id
    IS 'A foreign key to the predefined Concept identifier in the Standardized Vocabularies reflecting the validation status of the survey.';

COMMENT ON COLUMN cdm.survey_conduct.validated_survey_source_value
    IS 'Source value representing the validation status of the survey.';

COMMENT ON COLUMN cdm.survey_conduct.survey_version_number
    IS 'Version number of the questionnaire or survey used.';

COMMENT ON COLUMN cdm.survey_conduct.visit_occurrence_id
    IS 'A foreign key to the VISIT_OCCURRENCE table during which the survey was completed';

COMMENT ON COLUMN cdm.survey_conduct.visit_detail_id
    IS 'A foreign key to the Visit in the VISIT_DETAIL table when the Note was taken.';

COMMENT ON COLUMN cdm.survey_conduct.response_visit_occurrence_id
    IS 'A foreign key to the visit in the VISIT_OCCURRENCE table during which treatment was carried out that relates to this survey.';

GO
