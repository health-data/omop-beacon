//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,cdm.care_site.change0"
CREATE TABLE IF NOT EXISTS cdm.care_site_specialty
(
    care_site_id bigint references cdm.care_site(care_site_id) on update cascade on delete cascade,
    specialty_concept_id integer references vocabularies.concept(concept_id) on update cascade on delete no action,
    constraint xpk_care_site_specialty_id primary key (care_site_id)
);

GO

//// CHANGE name=change1
ALTER TABLE cdm.care_site_specialty
    ALTER CONSTRAINT care_site_specialty_care_site_id_fkey DEFERRABLE INITIALLY IMMEDIATE
  , ALTER CONSTRAINT care_site_specialty_specialty_concept_id_fkey DEFERRABLE INITIALLY IMMEDIATE
;

GO
