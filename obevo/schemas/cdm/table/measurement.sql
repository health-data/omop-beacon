//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0,provider.change0,"
CREATE TABLE IF NOT EXISTS cdm.measurement
(
    measurement_id bigint,
    person_id bigint NOT NULL,
    provider_id bigint,
    visit_occurrence_id bigint,
    visit_detail_id bigint,
    measurement_event_id bigint,
    measurement_datetime timestamp without time zone NOT NULL,
    measurement_concept_id integer NOT NULL,
    measurement_type_concept_id integer NOT NULL,
    measurement_source_concept_id integer NOT NULL,
    measurement_date date,
    operator_concept_id integer,
    value_as_concept_id integer,
    unit_concept_id integer,
    unit_source_concept_id integer,
    meas_event_field_concept_id integer,
    value_as_number numeric,
    range_low numeric,
    range_high numeric,
    measurement_time text
      CONSTRAINT chk_measurement_measurement_time CHECK ( coalesce(length(measurement_time), 0) <= 10 ),
    measurement_source_value text COLLATE pg_catalog."default",
    unit_source_value text COLLATE pg_catalog."default",
    value_source_value text COLLATE pg_catalog."default",
    CONSTRAINT xpk_measurement_id PRIMARY KEY (measurement_id),
    CONSTRAINT fpk_measurement_measurement_concept_id FOREIGN KEY (measurement_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_measurement_measurement_source_concept_id FOREIGN KEY (measurement_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_measurement_operator_concept_id FOREIGN KEY (operator_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_measurement_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_measurement_provider_id FOREIGN KEY (provider_id)
        REFERENCES cdm.provider (provider_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_measurement_measurement_type_concept_id FOREIGN KEY (measurement_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_measurement_unit_concept_id FOREIGN KEY (unit_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_measurement_value_as_concept_id FOREIGN KEY (value_as_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.measurement
    IS 'The MEASUREMENT table contains records of Measurement, i.e. structured values (numerical or categorical) obtained through systematic and standardized examination or testing of a Person or Person''s sample. The MEASUREMENT table contains both orders and results of such Measurements as laboratory tests, vital signs, quantitative findings from pathology reports, etc.';

COMMENT ON COLUMN cdm.measurement.measurement_id
    IS 'A unique identifier for each Measurement.';

COMMENT ON COLUMN cdm.measurement.person_id
    IS 'A foreign key identifier to the Person about whom the measurement was recorded. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.measurement.measurement_concept_id
    IS 'A foreign key to the standard measurement concept identifier in the Standardized Vocabularies. These belong to the ''Measurement'' domain, but could overlap with the ''Observation'' domain (see #3 below).';

COMMENT ON COLUMN cdm.measurement.measurement_date
    IS 'The date of the Measurement.';

COMMENT ON COLUMN cdm.measurement.measurement_datetime
    IS 'The date and time of the Measurement. Some database systems don''t have a datatype of time. To accommodate all temporal analyses, datatype datetime can be used (combining measurement_date and measurement_time [forum discussion](http://forums.ohdsi.org/t/date-time-and-datetime-problem-and-the-world-of-hours-and-1day/314))';

COMMENT ON COLUMN cdm.measurement.measurement_time
    IS 'The time of the Measurement. This is present for backwards compatibility and will be deprecated in an upcoming version';

COMMENT ON COLUMN cdm.measurement.measurement_type_concept_id
    IS 'A foreign key to the predefined Concept in the Standardized Vocabularies reflecting the provenance from where the Measurement record was recorded. These belong to the ''Meas Type'' vocabulary';

COMMENT ON COLUMN cdm.measurement.operator_concept_id
    IS 'A foreign key identifier to the predefined Concept in the Standardized Vocabularies reflecting the mathematical operator that is applied to the value_as_number. Operators are <, <=, =, >=, > and these concepts belong to the ''Meas Value Operator'' domain.';

COMMENT ON COLUMN cdm.measurement.value_as_number
    IS 'A Measurement result where the result is expressed as a numeric value.';

COMMENT ON COLUMN cdm.measurement.value_as_concept_id
    IS 'A foreign key to a Measurement result represented as a Concept from the Standardized Vocabularies (e.g., positive/negative, present/absent, low/high, etc.). These belong to the ''Meas Value'' domain';

COMMENT ON COLUMN cdm.measurement.unit_concept_id
    IS 'A foreign key to a Standard Concept ID of Measurement Units in the Standardized Vocabularies that belong to the ''Unit'' domain.';

COMMENT ON COLUMN cdm.measurement.range_low
    IS 'The lower limit of the normal range of the Measurement result. The lower range is assumed to be of the same unit of measure as the Measurement value.';

COMMENT ON COLUMN cdm.measurement.range_high
    IS 'The upper limit of the normal range of the Measurement. The upper range is assumed to be of the same unit of measure as the Measurement value.';

COMMENT ON COLUMN cdm.measurement.provider_id
    IS 'A foreign key to the provider in the PROVIDER table who was responsible for initiating or obtaining the measurement.';

COMMENT ON COLUMN cdm.measurement.visit_occurrence_id
    IS 'A foreign key to the Visit in the VISIT_OCCURRENCE table during which the Measurement was recorded.';

COMMENT ON COLUMN cdm.measurement.visit_detail_id
    IS 'A foreign key to the Visit Detail in the VISIT_DETAIL table during which the Measurement was recorded.';

COMMENT ON COLUMN cdm.measurement.measurement_source_value
    IS 'The Measurement name as it appears in the source data. This code is mapped to a Standard Concept in the Standardized Vocabularies and the original code is stored here for reference.';

COMMENT ON COLUMN cdm.measurement.measurement_source_concept_id
    IS 'A foreign key to a Concept in the Standard Vocabularies that refers to the code used in the source.';

COMMENT ON COLUMN cdm.measurement.unit_source_value
    IS 'The source code for the unit as it appears in the source data. This code is mapped to a standard unit concept in the Standardized Vocabularies and the original code is stored here for reference.';

COMMENT ON COLUMN cdm.measurement.value_source_value
    IS 'The source value associated with the content of the value_as_number or value_as_concept_id as stored in the source data.';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_measurement_measurement_concept_id
    ON cdm.measurement USING btree
    (measurement_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_measurement_person_id
    ON cdm.measurement USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change4
CREATE INDEX IF NOT EXISTS idx_measurement_visit_occurrence_id
    ON cdm.measurement USING btree
    (visit_occurrence_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change5
CREATE INDEX IF NOT EXISTS idx_measurement_measurement_date
    ON cdm.measurement USING btree
    (measurement_date ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change6
CREATE INDEX IF NOT EXISTS idx_measurement_value_as_concept_id
    ON cdm.measurement USING btree
    (value_as_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO


//// CHANGE name=change7
CREATE INDEX IF NOT EXISTS idx_measurement_measurement_source_concept_id
    ON cdm.measurement USING btree
    (measurement_source_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change8
CREATE INDEX IF NOT EXISTS idx_measurement_unit_concept_id
    ON cdm.measurement USING btree
    (unit_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change9
create index if not exists idx_measurement_measurement_datetime on cdm.measurement(measurement_datetime);
GO

//// CHANGE name=change10
ALTER TABLE cdm.measurement
ADD FOREIGN KEY (visit_occurrence_id)
  REFERENCES cdm.visit_occurrence (visit_occurrence_id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE NO ACTION
  DEFERRABLE
, ADD FOREIGN KEY (visit_detail_id)
  REFERENCES cdm.visit_detail (visit_detail_id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE NO ACTION
  DEFERRABLE
;

GO
