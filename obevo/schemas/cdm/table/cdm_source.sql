//// CHANGE name=change0 dependencies="public.00_migration.change0"
CREATE TABLE IF NOT EXISTS cdm.cdm_source
(
    cdm_version_concept_id integer,
    source_release_date date NOT NULL,
    cdm_release_date date,
    cdm_source_name text NOT NULL
      CONSTRAINT chk_cdm_source_cdm_source_name CHECK(length(cdm_source_name) <= 255),
    cdm_source_abbreviation text NOT NULL
      CONSTRAINT chk_cdm_source_cdm_source_abbreviation CHECK(length(cdm_source_abbreviation) <= 25),
    cdm_holder text NOT NULL
      CONSTRAINT chk_cdm_source_cdm_holder CHECK(length(cdm_holder) <= 255),
    source_description text,
    source_documentation_reference text
      CONSTRAINT chk_cdm_source_source_documentation_reference CHECK(coalesce(length(source_documentation_reference), 0) <= 255),
    cdm_etl_reference text
      CONSTRAINT chk_cdm_source_cdm_etl_reference CHECK(coalesce(length(cdm_etl_reference), 0) <= 255),
    cdm_version text
      CONSTRAINT chk_cdm_source_cdm_version CHECK(coalesce(length(cdm_version), 0) <= 10),
    vocabulary_version text
      CONSTRAINT chk_cdm_source_vocabulary_version CHECK(coalesce(length(vocabulary_version), 0) <= 20)
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.cdm_source
    IS 'The CDM_SOURCE table contains detail about the source database and the process used to transform the data into the OMOP Common Data Model.';

COMMENT ON COLUMN cdm.cdm_source.cdm_source_name
    IS 'The full name of the source';

COMMENT ON COLUMN cdm.cdm_source.cdm_source_abbreviation
    IS 'An abbreviation of the name';

COMMENT ON COLUMN cdm.cdm_source.cdm_holder
    IS 'The name of the organization responsible for the development of the CDM instance';

COMMENT ON COLUMN cdm.cdm_source.source_description
    IS 'A description of the source data origin and purpose for collection. The description may contain a summary of the period of time that is expected to be covered by this dataset.';

COMMENT ON COLUMN cdm.cdm_source.source_documentation_reference
    IS 'URL or other external reference to location of source documentation';

COMMENT ON COLUMN cdm.cdm_source.cdm_etl_reference
    IS 'URL or other external reference to location of ETL specification documentation and ETL source code';

COMMENT ON COLUMN cdm.cdm_source.source_release_date
    IS 'The date for which the source data are most current, such as the last day of data capture';

COMMENT ON COLUMN cdm.cdm_source.cdm_release_date
    IS 'The date when the CDM was instantiated';

COMMENT ON COLUMN cdm.cdm_source.cdm_version
    IS 'The version of CDM used';

COMMENT ON COLUMN cdm.cdm_source.vocabulary_version
    IS 'The version of the vocabulary used';

GO