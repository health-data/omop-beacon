//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0,payer_plan_period.change0,"
CREATE TABLE IF NOT EXISTS cdm.cost
(
    cost_id bigint,
    person_id bigint NOT NULL,
    cost_event_id bigint NOT NULL,
    payer_plan_period_id bigint,
    cost_event_field_concept_id integer NOT NULL,
    cost_concept_id integer NOT NULL,
    cost_type_concept_id integer NOT NULL,
    currency_concept_id integer NOT NULL,
    revenue_code_concept_id integer NOT NULL,
    drg_concept_id integer NOT NULL,
    cost_source_concept_id integer NOT NULL,
    incurred_date date NOT NULL,
    billed_date date,
    paid_date date,
    cost numeric,
    cost_source_value text COLLATE pg_catalog."default",
    revenue_code_source_value text COLLATE pg_catalog."default",
    drg_source_value text COLLATE pg_catalog."default",
    CONSTRAINT xpk_cost_id PRIMARY KEY (cost_id),
    CONSTRAINT fpk_cost_cost_concept_id FOREIGN KEY (cost_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_cost_currency_concept_id FOREIGN KEY (currency_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_cost_cost_event_field_concept_id FOREIGN KEY (cost_event_field_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_cost_payer_plan_period_id FOREIGN KEY (payer_plan_period_id)
        REFERENCES cdm.payer_plan_period (payer_plan_period_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_cost_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_cost_cost_source_concept_id FOREIGN KEY (cost_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_cost_cost_type_concept_id FOREIGN KEY (cost_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_cost_drg_concept_id FOREIGN KEY (drg_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_cost_revenue_code_concept_id FOREIGN KEY (revenue_code_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.cost
    IS 'The COST table captures records containing the cost of any medical event recorded in one of the OMOP clinical event tables such as DRUG_EXPOSURE, PROCEDURE_OCCURRENCE, VISIT_OCCURRENCE, VISIT_DETAIL, DEVICE_OCCURRENCE, OBSERVATION or MEASUREMENT.  Each record in the cost table account for the amount of money transacted for the clinical event. So, the COST table may be used to represent both receivables (charges) and payments (paid), each transaction type represented by its COST_CONCEPT_ID. The COST_TYPE_CONCEPT_ID field will use concepts in the Standardized Vocabularies to designate the source (provenance) of the cost data. A reference to the health plan information in the PAYER_PLAN_PERIOD table is stored in the record for information used for the adjudication system to determine the persons benefit for the clinical event.';

COMMENT ON COLUMN cdm.cost.cost_id
    IS 'A unique identifier for each COST record.';

COMMENT ON COLUMN cdm.cost.person_id
    IS 'A unique identifier for each PERSON.';

COMMENT ON COLUMN cdm.cost.cost_event_id
    IS 'A foreign key identifier to the event (e.g. Measurement, Procedure, Visit, Drug Exposure, etc) record for which cost data are recorded.';

COMMENT ON COLUMN cdm.cost.cost_event_field_concept_id
    IS 'A foreign key identifier to a concept in the CONCEPT table representing the identity of the field represented by COST_EVENT_ID';

COMMENT ON COLUMN cdm.cost.cost_concept_id
    IS 'A foreign key that refers to a Standard Cost Concept identifier in the Standardized Vocabularies belonging to the ''Cost'' vocabulary.';

COMMENT ON COLUMN cdm.cost.cost_type_concept_id
    IS 'A foreign key identifier to a concept in the CONCEPT table for the provenance or the source of the COST data and belonging to the ''Cost Type'' vocabulary';

COMMENT ON COLUMN cdm.cost.currency_concept_id
    IS 'A foreign key identifier to the concept representing the 3-letter code used to delineate international currencies, such as USD for US Dollar. These belong to the ''Currency'' vocabulary';

COMMENT ON COLUMN cdm.cost.cost
    IS 'The actual financial cost amount';

COMMENT ON COLUMN cdm.cost.incurred_date
    IS 'The first date of service of the clinical event corresponding to the cost as in table capturing the information (e.g. date of visit, date of procedure, date of condition, date of drug etc).';

COMMENT ON COLUMN cdm.cost.billed_date
    IS 'The date a bill was generated for a service or encounter';

COMMENT ON COLUMN cdm.cost.paid_date
    IS 'The date payment was received for a service or encounter';

COMMENT ON COLUMN cdm.cost.revenue_code_concept_id
    IS 'A foreign key referring to a Standard Concept ID in the Standardized Vocabularies for Revenue codes belonging to the ''Revenue Code'' vocabulary.';

COMMENT ON COLUMN cdm.cost.drg_concept_id
    IS 'A foreign key referring to a Standard Concept ID in the Standardized Vocabularies for DRG codes belonging to the ''DRG'' vocabulary.';

COMMENT ON COLUMN cdm.cost.cost_source_value
    IS 'The source value for the cost as it appears in the source data';

COMMENT ON COLUMN cdm.cost.cost_source_concept_id
    IS 'A foreign key to a Cost Concept that refers to the code used in the source.';

COMMENT ON COLUMN cdm.cost.revenue_code_source_value
    IS 'The source value for the Revenue code as it appears in the source data, stored here for reference.';

COMMENT ON COLUMN cdm.cost.drg_source_value
    IS 'The source value for the 3-digit DRG source code as it appears in the source data, stored here for reference.';

COMMENT ON COLUMN cdm.cost.payer_plan_period_id
    IS 'A foreign key to the PAYER_PLAN_PERIOD table, where the details of the Payer, Plan and Family are stored. Record the payer_plan_id that relates to the payer who contributed to the paid_by_payer field.';

GO
