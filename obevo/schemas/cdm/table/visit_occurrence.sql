//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,care_site.change0,person.change0,provider.change0"
CREATE TABLE IF NOT EXISTS cdm.visit_occurrence
(
    visit_occurrence_id bigint NOT NULL,
    person_id bigint NOT NULL,
    visit_start_datetime timestamp without time zone NOT NULL,
    visit_end_datetime timestamp without time zone NOT NULL,
    provider_id bigint,
    care_site_id bigint,
    preceding_visit_occurrence_id bigint,
    visit_concept_id integer NOT NULL,
    visit_type_concept_id integer NOT NULL,
    visit_source_concept_id integer NOT NULL,
    admitted_from_concept_id integer NOT NULL,
    discharged_to_concept_id integer NOT NULL,
    visit_start_date date,
    visit_end_date date,
    visit_source_value text COLLATE pg_catalog."default",
    admitted_from_source_value text COLLATE pg_catalog."default",
    discharged_to_source_value text COLLATE pg_catalog."default",
    CONSTRAINT xpk_visit_occurrence_id PRIMARY KEY (visit_occurrence_id),
    CONSTRAINT fpk_visit_occurrence_care_site_id FOREIGN KEY (care_site_id)
        REFERENCES cdm.care_site (care_site_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_occurrence_visit_concept_id FOREIGN KEY (visit_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_occurrence_visit_source_concept_id FOREIGN KEY (visit_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_occurrence_discharged_to_concept_id FOREIGN KEY (discharged_to_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_occurrence_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_occurrence_preceding_visit_occurrence_id FOREIGN KEY (preceding_visit_occurrence_id)
        REFERENCES cdm.visit_occurrence (visit_occurrence_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION,
    CONSTRAINT fpk_visit_occurrence_provider_id FOREIGN KEY (provider_id)
        REFERENCES cdm.provider (provider_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_occurrence_visit_type_concept_id FOREIGN KEY (visit_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1
COMMENT ON TABLE cdm.visit_occurrence
    IS 'The VISIT_OCCURRENCE table contains the spans of time a Person continuously receives medical services from one or more providers at a Care Site in a given setting within the health care system. Visits are classified into 4 settings: outpatient care, inpatient confinement, emergency room, and long-term care. Persons may transition between these settings over the course of an episode of care (for example, treatment of a disease onset).';

COMMENT ON COLUMN cdm.visit_occurrence.visit_occurrence_id
    IS 'A unique identifier for each Person''s visit or encounter at a healthcare provider.';

COMMENT ON COLUMN cdm.visit_occurrence.person_id
    IS 'A foreign key identifier to the Person for whom the visit is recorded. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.visit_occurrence.visit_concept_id
    IS 'A foreign key that refers to a visit Concept identifier in the Standardized Vocabularies belonging to the ''Visit'' Vocabulary.';

COMMENT ON COLUMN cdm.visit_occurrence.visit_start_date
    IS 'The start date of the visit.';

COMMENT ON COLUMN cdm.visit_occurrence.visit_start_datetime
    IS 'The date and time of the visit started.';

COMMENT ON COLUMN cdm.visit_occurrence.visit_end_date
    IS 'The end date of the visit. If this is a one-day visit the end date should match the start date.';

COMMENT ON COLUMN cdm.visit_occurrence.visit_end_datetime
    IS 'The date and time of the visit end.';

COMMENT ON COLUMN cdm.visit_occurrence.visit_type_concept_id
    IS 'A foreign key to the predefined Concept identifier in the Standardized Vocabularies reflecting the type of source data from which the visit record is derived belonging to the ''Visit Type'' vocabulary.';

COMMENT ON COLUMN cdm.visit_occurrence.provider_id
    IS 'A foreign key to the provider in the provider table who was associated with the visit.';

COMMENT ON COLUMN cdm.visit_occurrence.care_site_id
    IS 'A foreign key to the care site in the care site table that was visited.';

COMMENT ON COLUMN cdm.visit_occurrence.visit_source_value
    IS 'The source code for the visit as it appears in the source data.';

COMMENT ON COLUMN cdm.visit_occurrence.visit_source_concept_id
    IS 'A foreign key to a Concept that refers to the code used in the source.';

COMMENT ON COLUMN cdm.visit_occurrence.admitted_from_concept_id
    IS 'A foreign key to the predefined concept in the Place of Service Vocabulary reflecting where the patient was admitted from.';

COMMENT ON COLUMN cdm.visit_occurrence.admitted_from_source_value
    IS 'The source code for where the patient was admitted from as it appears in the source data.';

COMMENT ON COLUMN cdm.visit_occurrence.discharged_to_source_value
    IS 'The source code for the discharge disposition as it appears in the source data.';

COMMENT ON COLUMN cdm.visit_occurrence.discharged_to_concept_id
    IS 'A foreign key to the predefined concept in the Place of Service Vocabulary reflecting the discharge disposition for a visit.';

COMMENT ON COLUMN cdm.visit_occurrence.preceding_visit_occurrence_id
    IS 'A foreign key to the VISIT_OCCURRENCE table of the visit immediately preceding this visit';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_visit_occurrence_visit_concept_id
    ON cdm.visit_occurrence USING btree
    (visit_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_visit_occurrence_visit_source_concept_id
    ON cdm.visit_occurrence USING btree
    (visit_source_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change4
CREATE INDEX IF NOT EXISTS idx_visit_occurrence_discharged_to_concept_id
    ON cdm.visit_occurrence USING btree
    (discharged_to_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change5
CREATE INDEX IF NOT EXISTS idx_visit_occurrence_person_id
    ON cdm.visit_occurrence USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change6
CREATE INDEX IF NOT EXISTS idx_visit_occurrence_visit_type_concept_id
    ON cdm.visit_occurrence USING btree
    (visit_type_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change7
ALTER TABLE cdm.visit_occurrence
  ALTER CONSTRAINT fpk_visit_occurrence_preceding_visit_occurrence_id DEFERRABLE INITIALLY IMMEDIATE
;

GO

//// CHANGE name=change8
create index if not exists idx_visit_occurrence_visit_start_datetime on cdm.visit_occurrence(visit_start_datetime);
GO

//// CHANGE name=change9
CREATE INDEX IF NOT EXISTS idx_visit_occurrence_care_site_id ON cdm.visit_occurrence(care_site_id);
CREATE INDEX IF NOT EXISTS idx_visit_occurrence_preceding_visit_occurrence_id ON cdm.visit_occurrence(preceding_visit_occurrence_id);
GO


-- //// CHANGE name=change10
-- DROP INDEX IF EXISTS cdm.idx_visit_occurrence_discharged_to_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_visit_occurrence_visit_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_visit_occurrence_visit_source_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_visit_occurrence_visit_type_concept_id;

-- GO
