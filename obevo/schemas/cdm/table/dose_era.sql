//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0"
CREATE TABLE IF NOT EXISTS cdm.dose_era
(
    dose_era_id bigint NOT NULL,
    person_id bigint NOT NULL,
    dose_era_start_datetime timestamp without time zone NOT NULL,
    dose_era_end_datetime timestamp without time zone NOT NULL,
    drug_concept_id integer NOT NULL,
    unit_concept_id integer NOT NULL,
    dose_value numeric NOT NULL,
    CONSTRAINT xpk_dose_era_id PRIMARY KEY (dose_era_id),
    CONSTRAINT fpk_dose_era_drug_concept_id FOREIGN KEY (drug_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_dose_era_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_dose_era_unit_concept_id FOREIGN KEY (unit_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;
GO

//// CHANGE name=change1
COMMENT ON TABLE cdm.dose_era
    IS 'A Dose Era is defined as a span of time when the Person is assumed to be exposed to a constant dose of a specific active ingredient.';

COMMENT ON COLUMN cdm.dose_era.dose_era_id
    IS 'A unique identifier for each Dose Era.';

COMMENT ON COLUMN cdm.dose_era.person_id
    IS 'A foreign key identifier to the Person who is subjected to the drug during the drug era. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.dose_era.drug_concept_id
    IS 'A foreign key that refers to a Standard Concept identifier in the Standardized Vocabularies for the active Ingredient Concept.';

COMMENT ON COLUMN cdm.dose_era.unit_concept_id
    IS 'A foreign key that refers to a Standard Concept identifier in the Standardized Vocabularies for the unit concept.';

COMMENT ON COLUMN cdm.dose_era.dose_value
    IS 'The numeric value of the dose.';

COMMENT ON COLUMN cdm.dose_era.dose_era_start_datetime
    IS 'The start date for the drug era constructed from the individual instances of drug exposures. It is the start date of the very first chronologically recorded instance of utilization of a drug.';

COMMENT ON COLUMN cdm.dose_era.dose_era_end_datetime
    IS 'The end date for the drug era constructed from the individual instance of drug exposures. It is the end date of the final continuously recorded instance of utilization of a drug.';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_dose_era_drug_concept_id
    ON cdm.dose_era USING btree
    (drug_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_dose_era_person_id
    ON cdm.dose_era USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change4
create index if not exists idx_dose_era_dose_era_start_datetime on cdm.dose_era(dose_era_start_datetime);
GO
