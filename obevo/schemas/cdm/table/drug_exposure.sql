//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0,provider.change0"
CREATE TABLE IF NOT EXISTS cdm.drug_exposure
(
    drug_exposure_id bigint,
    person_id bigint NOT NULL,
    provider_id bigint,
    visit_occurrence_id bigint,
    visit_detail_id bigint,
    drug_exposure_start_datetime timestamp without time zone,
    drug_exposure_end_datetime timestamp without time zone,
    drug_concept_id integer NOT NULL,
    drug_type_concept_id integer NOT NULL,
    drug_source_concept_id integer NOT NULL,
    route_concept_id integer NOT NULL,
    refills integer,
    days_supply integer,
    drug_exposure_start_date date,
    drug_exposure_end_date date,
    verbatim_end_date date,
    quantity numeric,
    sig text COLLATE pg_catalog."default",
    lot_number text
      CONSTRAINT chk_drug_exposure_lot_number CHECK( coalesce(length(lot_number), 0) <= 50),
    drug_source_value text COLLATE pg_catalog."default",
    route_source_value text COLLATE pg_catalog."default",
    dose_unit_source_value text COLLATE pg_catalog."default",
    stop_reason text
      CONSTRAINT chk_drug_exposure_stop_reason CHECK( coalesce(length(stop_reason), 0) <= 256),
    CONSTRAINT xpk_drug_exposure_id PRIMARY KEY (drug_exposure_id),
    CONSTRAINT fpk_drug_exposure_drug_concept_id FOREIGN KEY (drug_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_drug_exposure_drug_source_concept_id FOREIGN KEY (drug_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_drug_exposure_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_drug_exposure_provider_id FOREIGN KEY (provider_id)
        REFERENCES cdm.provider (provider_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_drug_exposure_route_concept_id FOREIGN KEY (route_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_drug_exposure_drug_type_concept_id FOREIGN KEY (drug_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.drug_exposure
    IS 'The ''Drug'' domain captures records about the utilization of a Drug when ingested or otherwise introduced into the body. A Drug is a biochemical substance formulated in such a way that when administered to a Person it will exert a certain physiological effect. Drugs include prescription and over-the-counter medicines, vaccines, and large-molecule biologic therapies. Radiological devices ingested or applied locally do not count as Drugs.  Drug Exposure is inferred from clinical events associated with orders, prescriptions written, pharmacy dispensings, procedural administrations, and other patient-reported information, for example:  * The ''Prescription'' section of an EHR captures prescriptions written by physicians or from electronic ordering systems * The ''Medication list'' section of an EHR for both non-prescription products and medications prescribed by other providers * Prescriptions filled at dispensing providers such as pharmacies, and then captured in reimbursement claim systems * Drugs administered as part of a Procedure, such as chemotherapy or vaccines.';

COMMENT ON COLUMN cdm.drug_exposure.drug_exposure_id
    IS 'A system-generated unique identifier for each Drug utilization event.';

COMMENT ON COLUMN cdm.drug_exposure.person_id
    IS 'A foreign key identifier to the Person who is subjected to the Drug. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.drug_exposure.drug_concept_id
    IS 'A foreign key that refers to a Standard Concept identifier in the Standardized Vocabularies belonging to the ''Drug'' domain.';

COMMENT ON COLUMN cdm.drug_exposure.drug_exposure_start_date
    IS 'The start date for the current instance of Drug utilization. Valid entries include a start date of a prescription, the date a prescription was filled, or the date on which a Drug administration procedure was recorded.';

COMMENT ON COLUMN cdm.drug_exposure.drug_exposure_start_datetime
    IS 'The start date and time for the current instance of Drug utilization. Valid entries include a start datetime of a prescription, the date and time a prescription was filled, or the date and time on which a Drug administration procedure was recorded.';

COMMENT ON COLUMN cdm.drug_exposure.drug_exposure_end_date
    IS 'The end date for the current instance of Drug utilization. Depending on different sources, it could be a known or an inferred date and denotes the last day at which the patient was still exposed to Drug.';

COMMENT ON COLUMN cdm.drug_exposure.drug_exposure_end_datetime
    IS 'The end date and time for the current instance of Drug utilization. Depending on different sources, it could be a known or an inferred date and time and denotes the last day at which the patient was still exposed to Drug.';

COMMENT ON COLUMN cdm.drug_exposure.verbatim_end_date
    IS 'The known end date of a drug_exposure as provided by the source.';

COMMENT ON COLUMN cdm.drug_exposure.drug_type_concept_id
    IS 'A foreign key to the predefined Concept identifier in the Standardized Vocabularies reflecting the type of Drug Exposure recorded. It indicates how the Drug Exposure was represented in the source data and belongs to the ''Drug Type'' vocabulary.';

COMMENT ON COLUMN cdm.drug_exposure.refills
    IS 'The number of refills after the initial prescription. The initial prescription is not counted, values start with null.';

COMMENT ON COLUMN cdm.drug_exposure.quantity
    IS 'The quantity of drug as recorded in the original prescription or dispensing record.';

COMMENT ON COLUMN cdm.drug_exposure.days_supply
    IS 'The number of days of supply of the medication as prescribed. This reflects the intention of the provider for the length of exposure.';

COMMENT ON COLUMN cdm.drug_exposure.sig
    IS 'The directions (''signetur'') on the Drug prescription as recorded in the original prescription (and printed on the container) or dispensing record.';

COMMENT ON COLUMN cdm.drug_exposure.route_concept_id
    IS 'A foreign key that refers to a Standard Concept identifier in the Standardized Vocabularies reflecting the route of administration and belonging to the ''Route'' domain.';

COMMENT ON COLUMN cdm.drug_exposure.lot_number
    IS 'An identifier assigned to a particular quantity or lot of Drug product from the manufacturer.';

COMMENT ON COLUMN cdm.drug_exposure.provider_id
    IS 'A foreign key to the provider in the PROVIDER table who initiated (prescribed or administered) the Drug Exposure.';

COMMENT ON COLUMN cdm.drug_exposure.visit_occurrence_id
    IS 'A foreign key to the Visit in the VISIT_OCCURRENCE table during which the Drug Exposure was initiated.';

COMMENT ON COLUMN cdm.drug_exposure.visit_detail_id
    IS 'A foreign key to the Visit Detail in the VISIT_DETAIL table during which the Drug Exposure was initiated.';

COMMENT ON COLUMN cdm.drug_exposure.drug_source_value
    IS 'The source code for the Drug as it appears in the source data. This code is mapped to a Standard Drug concept in the Standardized Vocabularies and the original code is, stored here for reference.';

COMMENT ON COLUMN cdm.drug_exposure.drug_source_concept_id
    IS 'A foreign key to a Drug Concept that refers to the code used in the source.';

COMMENT ON COLUMN cdm.drug_exposure.route_source_value
    IS 'The information about the route of administration as detailed in the source.';

COMMENT ON COLUMN cdm.drug_exposure.dose_unit_source_value
    IS 'The information about the dose unit as detailed in the source.';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_drug_exposure_drug_concept_id
    ON cdm.drug_exposure USING btree
    (drug_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_drug_exposure_person_id
    ON cdm.drug_exposure USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change4
CREATE INDEX IF NOT EXISTS idx_drug_exposure_visit_occurrence_id
    ON cdm.drug_exposure USING btree
    (visit_occurrence_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change5
CREATE INDEX IF NOT EXISTS idx_drug_exposure_drug_source_concept_id
    ON cdm.drug_exposure USING btree
    (drug_source_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change6
CREATE INDEX IF NOT EXISTS idx_drug_exposure_route_concept_id
    ON cdm.drug_exposure USING btree
    (route_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change7
create index if not exists idx_drug_exposure_drug_exposure_start_datetime on cdm.drug_exposure(drug_exposure_start_datetime);
GO

//// CHANGE name=change8
ALTER TABLE cdm.drug_exposure
ADD FOREIGN KEY (visit_occurrence_id)
  REFERENCES cdm.visit_occurrence (visit_occurrence_id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE NO ACTION
  DEFERRABLE
, ADD FOREIGN KEY (visit_detail_id)
  REFERENCES cdm.visit_detail (visit_detail_id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE NO ACTION
  DEFERRABLE
;

GO
