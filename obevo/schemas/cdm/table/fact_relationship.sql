//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0"
CREATE TABLE IF NOT EXISTS cdm.fact_relationship
(
    fact_id_1 bigint NOT NULL,
    fact_id_2 bigint NOT NULL,
    domain_concept_id_1 integer NOT NULL,
    domain_concept_id_2 integer NOT NULL,
    relationship_concept_id integer NOT NULL,
    CONSTRAINT fpk_fact_relationship_domain_concept_id_1 FOREIGN KEY (domain_concept_id_1)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_fact_relationship_domain_concept_id_2 FOREIGN KEY (domain_concept_id_2)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_fact_relationship_relationship_concept_id FOREIGN KEY (relationship_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.fact_relationship
    IS 'The FACT_RELATIONSHIP table contains records about the relationships between facts stored as records in any table of the CDM. Relationships can be defined between facts from the same domain, or different domains. Examples of Fact Relationships include: Person relationships (parent-child), care site relationships (hierarchical organizational structure of facilities within a health system), indication relationship (between drug exposures and associated conditions), usage relationships (of devices during the course of an associated procedure), or facts derived from one another (measurements derived from an associated specimen).';

COMMENT ON COLUMN cdm.fact_relationship.domain_concept_id_1
    IS 'The concept representing the domain of fact one, from which the corresponding table can be inferred.';

COMMENT ON COLUMN cdm.fact_relationship.fact_id_1
    IS 'The unique identifier in the table corresponding to the domain of fact one.';

COMMENT ON COLUMN cdm.fact_relationship.domain_concept_id_2
    IS 'The concept representing the domain of fact two, from which the corresponding table can be inferred.';

COMMENT ON COLUMN cdm.fact_relationship.fact_id_2
    IS 'The unique identifier in the table corresponding to the domain of fact two.';

COMMENT ON COLUMN cdm.fact_relationship.relationship_concept_id
    IS 'A foreign key to a Standard Concept ID of relationship in the Standardized Vocabularies.';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_fact_relationship_domain_concept_id_1
    ON cdm.fact_relationship USING btree
    (domain_concept_id_1 ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_fact_relationship_domain_concept_id_2
    ON cdm.fact_relationship USING btree
    (domain_concept_id_2 ASC NULLS LAST)
    TABLESPACE pg_default;
GO

//// CHANGE name=change4
CREATE INDEX IF NOT EXISTS idx_fact_relationship_relationship_concept_id
    ON cdm.fact_relationship USING btree
    (relationship_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO