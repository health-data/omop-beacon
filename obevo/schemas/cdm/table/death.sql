//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0"
CREATE TABLE IF NOT EXISTS cdm.death
(
    person_id bigint,
    death_datetime timestamp without time zone,
    death_date date NOT NULL,
    death_type_concept_id integer NOT NULL,
    cause_concept_id integer,
    cause_source_concept_id integer,
    cause_source_value text
    CONSTRAINT chk_death_cause_source_value CHECK( coalesce(length(cause_source_value), 0) <= 256),
    CONSTRAINT xpk_death_id PRIMARY KEY (person_id),
    CONSTRAINT fpk_death_cause_concept_id FOREIGN KEY (cause_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_death_cause_source_concept_id FOREIGN KEY (cause_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_death_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_death_death_type_concept FOREIGN KEY (death_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0

COMMENT ON TABLE cdm.death
    IS 'The death domain contains the clinical event for how and when a Person dies (OMOP CDM v5).';

COMMENT ON COLUMN cdm.death.person_id
    IS 'A foreign key identifier to the deceased person. The demographic details of that person are stored in the person table.';

COMMENT ON COLUMN cdm.death.death_date
    IS 'The date the person was deceased.';

COMMENT ON COLUMN cdm.death.death_datetime
    IS 'The date and time the person was deceased.';

COMMENT ON COLUMN cdm.death.death_type_concept_id
    IS 'A foreign key referring to the predefined concept identifier in the Standardized Vocabularies reflecting how the death was represented in the source data.';

COMMENT ON COLUMN cdm.death.cause_concept_id
    IS 'A foreign key referring to a standard concept identifier in the Standardized Vocabularies for conditions.';

COMMENT ON COLUMN cdm.death.cause_source_concept_id
    IS 'A foreign key to the concept that refers to the code used in the source.';


GO
