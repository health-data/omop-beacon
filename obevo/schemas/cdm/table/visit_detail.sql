//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,care_site.change0,visit_occurrence.change0,person.change0,provider.change0"
CREATE TABLE IF NOT EXISTS cdm.visit_detail
(
    visit_detail_id bigint,
    visit_detail_start_datetime timestamp without time zone NOT NULL,
    visit_detail_end_datetime timestamp without time zone NOT NULL,
    person_id bigint NOT NULL,
    visit_occurrence_id bigint NOT NULL,
    provider_id bigint,
    care_site_id bigint,
    preceding_visit_detail_id bigint,
    parent_visit_detail_id bigint,
    visit_detail_concept_id integer NOT NULL,
    visit_detail_type_concept_id integer NOT NULL,
    discharged_to_concept_id integer NOT NULL,
    admitted_from_concept_id integer NOT NULL,
    visit_detail_source_concept_id integer NOT NULL,
    visit_detail_start_date date,
    visit_detail_end_date date,
    admitted_from_source_value text COLLATE pg_catalog."default",
    visit_detail_source_value text COLLATE pg_catalog."default",
    discharged_to_source_value text COLLATE pg_catalog."default",
    CONSTRAINT xpk_visit_detail_id PRIMARY KEY (visit_detail_id),
    CONSTRAINT fpk_visit_detail_visit_occurrence_id FOREIGN KEY (visit_occurrence_id)
        REFERENCES cdm.visit_occurrence (visit_occurrence_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_detail_care_site_id FOREIGN KEY (care_site_id)
        REFERENCES cdm.care_site (care_site_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_detail_visit_detail_concept_id FOREIGN KEY (visit_detail_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_detail_visit_detail_source_concept_id FOREIGN KEY (visit_detail_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_detail_discharged_to_concept_id FOREIGN KEY (discharged_to_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_detail_visit_detail_parent_id FOREIGN KEY (parent_visit_detail_id)
        REFERENCES cdm.visit_detail (visit_detail_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_detail_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_detail_preceding_visit_detail_id FOREIGN KEY (preceding_visit_detail_id)
        REFERENCES cdm.visit_detail (visit_detail_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_detail_provider_id FOREIGN KEY (provider_id)
        REFERENCES cdm.provider (provider_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_visit_detail_visit_detail_type_concept_id FOREIGN KEY (visit_detail_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.visit_detail
    IS 'The VISIT_DETAIL table is an optional table used to represents details of each record in the parent visit_occurrence table. For every record in visit_occurrence table there may be 0 or more records in the visit_detail table with a 1:n relationship where n may be 0. The visit_detail table is structurally very similar to visit_occurrence table and belongs to the similar domain as the visit.';

COMMENT ON COLUMN cdm.visit_detail.visit_detail_id
    IS 'A unique identifier for each Person''s visit or encounter at a healthcare provider.';

COMMENT ON COLUMN cdm.visit_detail.person_id
    IS 'A foreign key identifier to the Person for whom the visit is recorded. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.visit_detail.visit_detail_concept_id
    IS 'A foreign key that refers to a visit Concept identifier in the Standardized Vocabularies belonging to the ''Visit'' Vocabulary.';

COMMENT ON COLUMN cdm.visit_detail.visit_detail_start_date
    IS 'The start date of the visit.';

COMMENT ON COLUMN cdm.visit_detail.visit_detail_start_datetime
    IS 'The date and time of the visit started.';

COMMENT ON COLUMN cdm.visit_detail.visit_detail_end_date
    IS 'The end date of the visit. If this is a one-day visit the end date should match the start date.';

COMMENT ON COLUMN cdm.visit_detail.visit_detail_end_datetime
    IS 'The date and time of the visit end.';

COMMENT ON COLUMN cdm.visit_detail.visit_detail_type_concept_id
    IS 'A foreign key to the predefined Concept identifier in the Standardized Vocabularies reflecting the type of source data from which the visit record is derived belonging to the ''Visit Type'' vocabulary.';

COMMENT ON COLUMN cdm.visit_detail.provider_id
    IS 'A foreign key to the provider in the provider table who was associated with the visit.';

COMMENT ON COLUMN cdm.visit_detail.care_site_id
    IS 'A foreign key to the care site in the care site table that was visited.';

COMMENT ON COLUMN cdm.visit_detail.discharged_to_concept_id
    IS 'A foreign key to the predefined concept in the ''Place of Service'' Vocabulary reflecting the discharge disposition for a visit.';

COMMENT ON COLUMN cdm.visit_detail.admitted_from_concept_id
    IS 'A foreign key to the predefined concept in the ''Place of Service'' Vocabulary reflecting the admitting source for a visit.';

COMMENT ON COLUMN cdm.visit_detail.admitted_from_source_value
    IS 'The source code for the admitting source as it appears in the source data.';

COMMENT ON COLUMN cdm.visit_detail.visit_detail_source_value
    IS 'The source code for the visit as it appears in the source data.';

COMMENT ON COLUMN cdm.visit_detail.visit_detail_source_concept_id
    IS 'A foreign key to a Concept that refers to the code used in the source.';

COMMENT ON COLUMN cdm.visit_detail.discharged_to_source_value
    IS 'The source code for the discharge disposition as it appears in the source data.';

COMMENT ON COLUMN cdm.visit_detail.preceding_visit_detail_id
    IS 'A foreign key to the VISIT_DETAIL table of the visit immediately preceding this visit';

COMMENT ON COLUMN cdm.visit_detail.parent_visit_detail_id
    IS 'A foreign key to the VISIT_DETAIL table record to represent the immediate parent visit-detail record.';

COMMENT ON COLUMN cdm.visit_detail.visit_occurrence_id
    IS 'A foreign key that refers to the record in the VISIT_OCCURRENCE table. This is a required field, because for every visit_detail is a child of visit_occurrence and cannot exist without a corresponding parent record in visit_occurrence.';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_visit_detail_care_site_id
    ON cdm.visit_detail USING btree
    (care_site_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO


//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_visit_detail_visit_detail_concept_id
    ON cdm.visit_detail USING btree
    (visit_detail_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO


//// CHANGE name=change4
CREATE INDEX IF NOT EXISTS idx_visit_detail_visit_detail_source_concept_id
    ON cdm.visit_detail USING btree
    (visit_detail_source_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO


//// CHANGE name=change5
CREATE INDEX IF NOT EXISTS idx_visit_detail_discharged_to_concept_id
    ON cdm.visit_detail USING btree
    (discharged_to_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO


//// CHANGE name=change6
CREATE INDEX IF NOT EXISTS idx_visit_detail_person_id
    ON cdm.visit_detail USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO


//// CHANGE name=change7
CREATE INDEX IF NOT EXISTS idx_visit_detail_visit_detail_type_concept_id
    ON cdm.visit_detail USING btree
    (visit_detail_type_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change8
ALTER TABLE cdm.visit_detail
	alter column visit_detail_start_datetime drop not null
	, alter column visit_detail_end_datetime drop not null
	, alter column visit_detail_source_concept_id drop not null
	, alter column admitted_from_concept_id drop not null
	, alter column discharged_to_concept_id drop not null
;

GO

//// CHANGE name=change9
create index if not exists idx_visit_detail_visit_detail_start_datetime on cdm.visit_detail(visit_detail_start_datetime);
GO

//// CHANGE name=change10
CREATE INDEX IF NOT EXISTS idx_visit_detail_preceding_visit_detail_id ON cdm.visit_detail(preceding_visit_detail_id);
CREATE INDEX IF NOT EXISTS idx_visit_detail_parent_visit_detail_id ON cdm.visit_detail(parent_visit_detail_id);
CREATE INDEX IF NOT EXISTS idx_visit_detail_visit_occurrence_id ON cdm.visit_detail(visit_occurrence_id);
GO

-- //// CHANGE name=change11
-- DROP INDEX IF EXISTS cdm.idx_visit_detail_discharged_to_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_visit_detail_visit_detail_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_visit_detail_visit_detail_source_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_visit_detail_visit_detail_type_concept_id;

-- GO

