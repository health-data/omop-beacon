//// CHANGE name=change0 dependencies="public.00_migration.change0"
CREATE TABLE IF NOT EXISTS cdm.location
(
    location_id bigint,
    country_concept_id integer,
    latitude numeric,
    longitude numeric,
    address_1 character varying(50) COLLATE pg_catalog."default",
    address_2 character varying(50) COLLATE pg_catalog."default",
    city character varying(50) COLLATE pg_catalog."default",
    state character varying(2) COLLATE pg_catalog."default",
    zip character varying(9) COLLATE pg_catalog."default",
    county character varying(20) COLLATE pg_catalog."default",
    location_source_value text COLLATE pg_catalog."default",
    country_source_value character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT xpk_location_id PRIMARY KEY (location_id)
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1
COMMENT ON TABLE cdm.location
    IS 'The LOCATION table represents a generic way to capture physical location or address information of Persons and Care Sites.';

COMMENT ON COLUMN cdm.location.location_id
    IS 'A unique identifier for each geographic location.';

COMMENT ON COLUMN cdm.location.address_1
    IS 'The address field 1, typically used for the street address, as it appears in the source data.';

COMMENT ON COLUMN cdm.location.address_2
    IS 'The address field 2, typically used for additional detail such as buildings, suites, floors, as it appears in the source data.';

COMMENT ON COLUMN cdm.location.city
    IS 'The city field as it appears in the source data.';

COMMENT ON COLUMN cdm.location.state
    IS 'The state field as it appears in the source data.';

COMMENT ON COLUMN cdm.location.zip
    IS 'The zip or postal code.';

COMMENT ON COLUMN cdm.location.county
    IS 'The county.';

COMMENT ON COLUMN cdm.location.country_source_value
    IS 'The country';

COMMENT ON COLUMN cdm.location.location_source_value
    IS 'The verbatim information that is used to uniquely identify the location as it appears in the source data.';

COMMENT ON COLUMN cdm.location.latitude
    IS 'The geocoded latitude';

COMMENT ON COLUMN cdm.location.longitude
    IS 'The geocoded longitude';

GO

