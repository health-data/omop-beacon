//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0"
CREATE TABLE IF NOT EXISTS cdm.specimen
(
    specimen_id bigint,
    person_id bigint NOT NULL,
    specimen_datetime timestamp without time zone NOT NULL,
    specimen_concept_id integer NOT NULL,
    specimen_type_concept_id integer NOT NULL,
    anatomic_site_concept_id integer NOT NULL,
    disease_status_concept_id integer NOT NULL,
    unit_concept_id integer,
    specimen_date date,
    quantity numeric,
    specimen_source_id  text
      CONSTRAINT chk_specimen_specimen_source_id CHECK (coalesce(length(specimen_source_id), 0) <= 50),
    specimen_source_value text COLLATE pg_catalog."default",
    unit_source_value text COLLATE pg_catalog."default",
    anatomic_site_source_value text COLLATE pg_catalog."default",
    disease_status_source_value text COLLATE pg_catalog."default",
    CONSTRAINT xpk_specimen PRIMARY KEY (specimen_id),
    CONSTRAINT fpk_specimen_specimen_concept_id FOREIGN KEY (specimen_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_specimen_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_specimen_anatomic_site_concept_id FOREIGN KEY (anatomic_site_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_specimen_disease_status_concept_id FOREIGN KEY (disease_status_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_specimen_specimen_type_concept_id FOREIGN KEY (specimen_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_specimen_unit_concept_id FOREIGN KEY (unit_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.specimen
    IS 'The specimen domain contains the records identifying biological samples from a person.';

COMMENT ON COLUMN cdm.specimen.specimen_id
    IS 'A unique identifier for each specimen.';

COMMENT ON COLUMN cdm.specimen.person_id
    IS 'A foreign key identifier to the Person for whom the Specimen is recorded.';

COMMENT ON COLUMN cdm.specimen.specimen_concept_id
    IS 'A foreign key referring to a Standard Concept identifier in the Standardized Vocabularies for the Specimen.';

COMMENT ON COLUMN cdm.specimen.specimen_type_concept_id
    IS 'A foreign key referring to the Concept identifier in the Standardized Vocabularies reflecting the system of record from which the Specimen was represented in the source data.';

COMMENT ON COLUMN cdm.specimen.specimen_date
    IS 'The date the specimen was obtained from the Person.';

COMMENT ON COLUMN cdm.specimen.specimen_datetime
    IS 'The date and time on the date when the Specimen was obtained from the person.';

COMMENT ON COLUMN cdm.specimen.quantity
    IS 'The amount of specimen collection from the person during the sampling procedure.';

COMMENT ON COLUMN cdm.specimen.unit_concept_id
    IS 'A foreign key to a Standard Concept identifier for the Unit associated with the numeric quantity of the Specimen collection.';

COMMENT ON COLUMN cdm.specimen.anatomic_site_concept_id
    IS 'A foreign key to a Standard Concept identifier for the anatomic location of specimen collection.';

COMMENT ON COLUMN cdm.specimen.disease_status_concept_id
    IS 'A foreign key to a Standard Concept identifier for the Disease Status of specimen collection.';

COMMENT ON COLUMN cdm.specimen.specimen_source_id
    IS 'The Specimen identifier as it appears in the source data.';

COMMENT ON COLUMN cdm.specimen.specimen_source_value
    IS 'The Specimen value as it appears in the source data. This value is mapped to a Standard Concept in the Standardized Vocabularies and the original code is, stored here for reference.';

COMMENT ON COLUMN cdm.specimen.unit_source_value
    IS 'The information about the Unit as detailed in the source.';

COMMENT ON COLUMN cdm.specimen.anatomic_site_source_value
    IS 'The information about the anatomic site as detailed in the source.';

COMMENT ON COLUMN cdm.specimen.disease_status_source_value
    IS 'The information about the disease status as detailed in the source.';

GO

//// CHANGE name=change2
create index if not exists idx_specimen_specimen_datetime on cdm.specimen(specimen_datetime);
GO
