//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0"
CREATE TABLE IF NOT EXISTS cdm.metadata
(
    metadata_datetime timestamp without time zone,
    metadata_concept_id integer NOT NULL,
    metadata_type_concept_id integer NOT NULL,
    value_as_concept_id integer,
    metadata_date date,
    name text NOT NULL
      CONSTRAINT chk_metadata_name CHECK ( length(name) <= 250 ),
    value_as_string text COLLATE pg_catalog."default",
    CONSTRAINT fpk_metadata_metadata_concept_id FOREIGN KEY (metadata_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_metadata_metadata_type_concept FOREIGN KEY (metadata_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.metadata
    IS 'The METADATA table contains metadata information about a dataset that has been transformed to the OMOP Common Data Model.';

COMMENT ON COLUMN cdm.metadata.metadata_concept_id
    IS 'A foreign key that refers to a Standard Metadata Concept identifier in the Standardized Vocabularies.';

COMMENT ON COLUMN cdm.metadata.metadata_type_concept_id
    IS 'A foreign key that refers to a Standard Type Concept identifier in the Standardized Vocabularies.';

COMMENT ON COLUMN cdm.metadata.name
    IS 'The name of the Concept stored in metadata_concept_id or a description of the data being stored.';

COMMENT ON COLUMN cdm.metadata.value_as_string
    IS 'The metadata value stored as a string.';

COMMENT ON COLUMN cdm.metadata.value_as_concept_id
    IS 'A foreign key to a metadata value stored as a Concept ID.';

COMMENT ON COLUMN cdm.metadata.metadata_date
    IS 'The date associated with the metadata';

COMMENT ON COLUMN cdm.metadata.metadata_datetime
    IS 'The date and time associated with the metadata';

GO