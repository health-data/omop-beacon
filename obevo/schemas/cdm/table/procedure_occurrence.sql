//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0,provider.change0,visit_detail.change0,visit_occurrence.change0"
CREATE TABLE IF NOT EXISTS cdm.procedure_occurrence
(
    procedure_occurrence_id bigint,
    person_id bigint NOT NULL,
    procedure_datetime timestamp without time zone NOT NULL,
    provider_id bigint,
    visit_occurrence_id bigint,
    visit_detail_id bigint,
    procedure_end_datetime timestamp without time zone,
    procedure_concept_id integer NOT NULL,
    procedure_source_concept_id integer NOT NULL,
    procedure_type_concept_id integer NOT NULL,
    modifier_concept_id integer NOT NULL,
    quantity integer,
    procedure_date date,
    procedure_end_date date,
    procedure_source_value text COLLATE pg_catalog."default",
    modifier_source_value text COLLATE pg_catalog."default",
    CONSTRAINT xpk_procedure_occurrence_id PRIMARY KEY (procedure_occurrence_id),
    CONSTRAINT fpk_procedure_occurrence_procedure_concept_id FOREIGN KEY (procedure_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_procedure_occurrence_procedure_source_concept_id FOREIGN KEY (procedure_source_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_procedure_occurrence_modifier_concept_id FOREIGN KEY (modifier_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_procedure_occurrence_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_procedure_occurrence_provider_id FOREIGN KEY (provider_id)
        REFERENCES cdm.provider (provider_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_procedure_occurrence_procedure_type_concept_id FOREIGN KEY (procedure_type_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_procedure_occurrence_visit_detail_id FOREIGN KEY (visit_detail_id)
        REFERENCES cdm.visit_detail (visit_detail_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_procedure_occurrence_visit_occurrence_id FOREIGN KEY (visit_occurrence_id)
        REFERENCES cdm.visit_occurrence (visit_occurrence_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1
COMMENT ON TABLE cdm.procedure_occurrence
    IS 'The PROCEDURE_OCCURRENCE table contains records of activities or processes ordered by, or carried out by, a healthcare provider on the patient to have a diagnostic or therapeutic purpose. Procedures are present in various data sources in different forms with varying levels of standardization. For example:  * Medical Claims include procedure codes that are submitted as part of a claim for health services rendered, including procedures performed. * Electronic Health Records that capture procedures as orders.';

COMMENT ON COLUMN cdm.procedure_occurrence.procedure_occurrence_id
    IS 'A system-generated unique identifier for each Procedure Occurrence.';

COMMENT ON COLUMN cdm.procedure_occurrence.person_id
    IS 'A foreign key identifier to the Person who is subjected to the Procedure. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.procedure_occurrence.procedure_concept_id
    IS 'A foreign key that refers to a standard procedure Concept identifier in the Standardized Vocabularies.';

COMMENT ON COLUMN cdm.procedure_occurrence.procedure_date
    IS 'The date on which the Procedure was performed.';

COMMENT ON COLUMN cdm.procedure_occurrence.procedure_datetime
    IS 'The date and time on which the Procedure was performed.';

COMMENT ON COLUMN cdm.procedure_occurrence.procedure_type_concept_id
    IS 'A foreign key to the predefined Concept identifier in the Standardized Vocabularies reflecting the type of source data from which the procedure record is derived, belonging to the ''Procedure Type'' vocabulary.';

COMMENT ON COLUMN cdm.procedure_occurrence.modifier_concept_id
    IS 'A foreign key to a Standard Concept identifier for a modifier to the Procedure (e.g. bilateral). These concepts are typically distinguished by ''Modifier'' concept classes (e.g., ''CPT4 Modifier'' as part of the ''CPT4'' vocabulary).';

COMMENT ON COLUMN cdm.procedure_occurrence.quantity
    IS 'The quantity of procedures ordered or administered.';

COMMENT ON COLUMN cdm.procedure_occurrence.provider_id
    IS 'A foreign key to the provider in the PROVIDER table who was responsible for carrying out the procedure.';

COMMENT ON COLUMN cdm.procedure_occurrence.visit_occurrence_id
    IS 'A foreign key to the Visit in the VISIT_OCCURRENCE table during which the Procedure was carried out.';

COMMENT ON COLUMN cdm.procedure_occurrence.visit_detail_id
    IS 'A foreign key to the Visit Detail in the VISIT_DETAIL table during which the Procedure was carried out.';

COMMENT ON COLUMN cdm.procedure_occurrence.procedure_source_value
    IS 'The source code for the Procedure as it appears in the source data. This code is mapped to a standard procedure Concept in the Standardized Vocabularies and the original code is, stored here for reference. Procedure source codes are typically ICD-9-Proc, CPT-4, HCPCS or OPCS-4 codes.';

COMMENT ON COLUMN cdm.procedure_occurrence.procedure_source_concept_id
    IS 'A foreign key to a Procedure Concept that refers to the code used in the source.';

COMMENT ON COLUMN cdm.procedure_occurrence.modifier_source_value
    IS 'The source code for the qualifier as it appears in the source data.';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_procedure_occurrence_procedure_concept_id
    ON cdm.procedure_occurrence USING btree
    (procedure_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_procedure_occurrence_procedure_source_concept_id
    ON cdm.procedure_occurrence USING btree
    (procedure_source_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change4
CREATE INDEX IF NOT EXISTS idx_procedure_occurrence_modifier_concept_id
    ON cdm.procedure_occurrence USING btree
    (modifier_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change5
CREATE INDEX IF NOT EXISTS idx_procedure_occurrence_procedure_type_concept_id
    ON cdm.procedure_occurrence USING btree
    (procedure_type_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change6
CREATE INDEX IF NOT EXISTS idx_procedure_occurrence_person_id
    ON cdm.procedure_occurrence USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change7
create index if not exists idx_procedure_occurrence_procedure_datetime on cdm.procedure_occurrence(procedure_datetime);
GO

//// CHANGE name=change8
CREATE INDEX IF NOT EXISTS idx_procedure_occurrence_visit_detail_id ON cdm.procedure_occurrence(visit_detail_id);
CREATE INDEX IF NOT EXISTS idx_procedure_occurrence_visit_occurrence_id ON cdm.procedure_occurrence(visit_occurrence_id);
GO

-- //// CHANGE name=change9
-- DROP INDEX IF EXISTS cdm.idx_procedure_occurrence_modifier_concept_id;
-- DROP INDEX IF EXISTS cdm.idx_procedure_occurrence_procedure_type_concept_id;

-- GO