//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0"
CREATE TABLE IF NOT EXISTS cdm.condition_era
(
    condition_era_id bigint,
    person_id bigint NOT NULL,
    condition_concept_id integer NOT NULL,
    condition_occurrence_count integer,
    condition_era_start_date date,
    condition_era_end_date date,
    CONSTRAINT xpk_condition_era_id PRIMARY KEY (condition_era_id),
    CONSTRAINT fpk_condition_era_condition_concept_id FOREIGN KEY (condition_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_condition_era_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1 dependencies=change0
COMMENT ON TABLE cdm.condition_era
    IS 'A Condition Era is defined as a span of time when the Person is assumed to have a given condition. Similar to Drug Eras, Condition Eras are chronological periods of Condition Occurrence. Combining individual Condition Occurrences into a single Condition Era serves two purposes:  * It allows aggregation of chronic conditions that require frequent ongoing care, instead of treating each Condition Occurrence as an independent event. * It allows aggregation of multiple, closely timed doctor visits for the same Condition to avoid double-counting the Condition Occurrences.  For example, consider a Person who visits her Primary Care Physician (PCP) and who is referred to a specialist. At a later time, the Person visits the specialist, who confirms the PCP''s original diagnosis and provides the appropriate treatment to resolve the condition. These two independent doctor visits should be aggregated into one Condition Era.';

COMMENT ON COLUMN cdm.condition_era.condition_era_id
    IS 'A unique identifier for each Condition Era.';

COMMENT ON COLUMN cdm.condition_era.person_id
    IS 'A foreign key identifier to the Person who is experiencing the Condition during the Condition Era. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.condition_era.condition_concept_id
    IS 'A foreign key that refers to a standard Condition Concept identifier in the Standardized Vocabularies.';

COMMENT ON COLUMN cdm.condition_era.condition_occurrence_count
    IS 'The number of individual Condition Occurrences used to construct the condition era.';

COMMENT ON COLUMN cdm.condition_era.condition_era_start_date
    IS 'The start date for the Condition Era constructed from the individual instances of Condition Occurrences.';

COMMENT ON COLUMN cdm.condition_era.condition_era_end_date
    IS 'The end date for the Condition Era constructed from the individual instances of Condition Occurrences.';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_condition_era_condition_concept_id
    ON cdm.condition_era USING btree
    (condition_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_condition_era_person_id
    ON cdm.condition_era USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO
