//// CHANGE name=change0 dependencies="vocabularies.concept.change0,public.00_migration.change0,person.change0"
CREATE TABLE IF NOT EXISTS cdm.drug_era
(
    drug_era_id bigint NOT NULL,
    person_id bigint NOT NULL,
    drug_concept_id integer NOT NULL,
    drug_exposure_count integer,
    gap_days integer,
    drug_era_start_date date,
    drug_era_end_date date,
    CONSTRAINT xpk_drug_era_id PRIMARY KEY (drug_era_id),
    CONSTRAINT fpk_drug_era_drug_concept_id FOREIGN KEY (drug_concept_id)
        REFERENCES vocabularies.concept (concept_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE,
    CONSTRAINT fpk_drug_era_person_id FOREIGN KEY (person_id)
        REFERENCES cdm.person (person_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE
)
TABLESPACE pg_default;

GO

//// CHANGE name=change1
COMMENT ON TABLE cdm.drug_era
    IS 'A Drug Era is defined as a span of time when the Person is assumed to be exposed to a particular active ingredient. A Drug Era is not the same as a Drug Exposure: Exposures are individual records corresponding to the source when Drug was delivered to the Person, while successive periods of Drug Exposures are combined under certain rules to produce continuous Drug Eras.';

COMMENT ON COLUMN cdm.drug_era.drug_era_id
    IS 'A unique identifier for each Drug Era.';

COMMENT ON COLUMN cdm.drug_era.person_id
    IS 'A foreign key identifier to the Person who is subjected to the Drug during the fDrug Era. The demographic details of that Person are stored in the PERSON table.';

COMMENT ON COLUMN cdm.drug_era.drug_concept_id
    IS 'A foreign key that refers to a Standard Concept identifier in the Standardized Vocabularies for the Ingredient Concept.';

COMMENT ON COLUMN cdm.drug_era.drug_exposure_count
    IS 'The number of individual Drug Exposure occurrences used to construct the Drug Era.';

COMMENT ON COLUMN cdm.drug_era.gap_days
    IS 'The number of days that are not covered by DRUG_EXPOSURE records that were used to make up the era record.';

COMMENT ON COLUMN cdm.drug_era.drug_era_start_date
    IS 'The start date for the Drug Era constructed from the individual instances of Drug Exposures.';

COMMENT ON COLUMN cdm.drug_era.drug_era_end_date
    IS 'The end date for the drug era constructed from the individual instance of drug exposures.';

GO

//// CHANGE name=change2
CREATE INDEX IF NOT EXISTS idx_drug_era_drug_concept_id
    ON cdm.drug_era USING btree
    (drug_concept_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO

//// CHANGE name=change3
CREATE INDEX IF NOT EXISTS idx_drug_era_person_id
    ON cdm.drug_era USING btree
    (person_id ASC NULLS LAST)
    TABLESPACE pg_default;

GO