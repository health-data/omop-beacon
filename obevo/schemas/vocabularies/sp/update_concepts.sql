
CREATE OR REPLACE PROCEDURE vocabularies.update_concepts(
	)
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
	source_schema text;
	source_table text;
	source_column text;
	qry text;
BEGIN

	SET CONSTRAINTS ALL DEFERRED;

	-- Vocabularies, domains, concept_class and relationship
	-- are deleted at the end of the procedure
	RAISE NOTICE '% Updating vocabulary', clock_timestamp();
	UPDATE vocabularies.vocabulary ovc
	SET
		  vocabulary_name = vc.vocabulary_name
		, vocabulary_reference = vc.vocabulary_reference
		, vocabulary_version = vc.vocabulary_version
		, vocabulary_concept_id = vc.vocabulary_concept_id
	FROM vocabulary_updates.vocabulary vc
	WHERE ovc.vocabulary_id = vc.vocabulary_id
    AND (
         ovc.vocabulary_name <> vc.vocabulary_name
      OR ovc.vocabulary_reference <> vc.vocabulary_reference
      OR ovc.vocabulary_version <> vc.vocabulary_version
      OR ovc.vocabulary_concept_id <> vc.vocabulary_concept_id
    );

	RAISE NOTICE '% Inserting vocabulary', clock_timestamp();
	INSERT INTO vocabularies.vocabulary(
      vocabulary_id
    , vocabulary_name
    , vocabulary_reference
    , vocabulary_version
    , vocabulary_concept_id
    )
	SELECT
      vc.vocabulary_id
    , vc.vocabulary_name
    , vc.vocabulary_reference
    , vc.vocabulary_version
    , vc.vocabulary_concept_id
	FROM vocabulary_updates.vocabulary vc
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabularies.vocabulary ovc
		WHERE ovc.vocabulary_id = vc.vocabulary_id
	);

	RAISE NOTICE '% Updating domain', clock_timestamp();
	UPDATE vocabularies.domain odm
	SET
      domain_name = dm.domain_name
		, domain_concept_id = dm.domain_concept_id
	FROM vocabulary_updates.domain dm
	WHERE odm.domain_id = dm.domain_id
    AND (
      odm.domain_name <> dm.domain_name
      OR odm.domain_concept_id <> dm.domain_concept_id
    );

	RAISE NOTICE '% Inserting domain', clock_timestamp();
	INSERT INTO vocabularies.domain(domain_id, domain_name, domain_concept_id)
	SELECT dm.domain_id, dm.domain_name, dm.domain_concept_id
	FROM vocabulary_updates.domain dm
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabularies.domain odm
		WHERE odm.domain_id = dm.domain_id
	);

	RAISE NOTICE '% Updating concept_class', clock_timestamp();
	UPDATE vocabularies.concept_class occ
	SET
		  concept_class_name = cc.concept_class_name
		, concept_class_concept_id = cc.concept_class_concept_id
	FROM vocabulary_updates.concept_class cc
	WHERE
		occ.concept_class_id = cc.concept_class_id
    AND (
      occ.concept_class_name <> cc.concept_class_name
      OR occ.concept_class_concept_id <> cc.concept_class_concept_id
    );

	RAISE NOTICE '% Inserting concept_class', clock_timestamp();
	INSERT INTO vocabularies.concept_class(concept_class_id, concept_class_name, concept_class_concept_id)
	SELECT cc.concept_class_id, cc.concept_class_name, cc.concept_class_concept_id
	FROM vocabulary_updates.concept_class cc
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabularies.concept_class occ
		WHERE occ.concept_class_id = cc.concept_class_id
	);

	RAISE NOTICE '% Updating relationship', clock_timestamp();
	UPDATE vocabularies.relationship ors
	SET
		  relationship_name = rs.relationship_name
		, is_hierarchical = rs.is_hierarchical
		, defines_ancestry = rs.defines_ancestry
		, reverse_relationship_id = rs.reverse_relationship_id
		, relationship_concept_id = rs.relationship_concept_id
	FROM vocabulary_updates.relationship rs
	WHERE
		ors.relationship_id = rs.relationship_id
		AND (
         ors.relationship_name <> rs.relationship_name
  		OR ors.is_hierarchical <> rs.is_hierarchical
  		OR ors.defines_ancestry <> rs.defines_ancestry
  		OR ors.reverse_relationship_id <> rs.reverse_relationship_id
  		OR ors.relationship_concept_id <> rs.relationship_concept_id
        );

	RAISE NOTICE '% Inserting relationship', clock_timestamp();
	INSERT INTO vocabularies.relationship(
    relationship_id, relationship_name, is_hierarchical
  , defines_ancestry, reverse_relationship_id, relationship_concept_id
  )
	SELECT
      rs.relationship_id, rs.relationship_name
    , rs.is_hierarchical, rs.defines_ancestry
    , rs.reverse_relationship_id, rs.relationship_concept_id
	FROM vocabulary_updates.relationship rs
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabularies.relationship ors
		WHERE ors.relationship_id = rs.relationship_id
	);

	RAISE NOTICE '% Updating concept', clock_timestamp();
	UPDATE vocabularies.concept cp
	SET concept_name = dc.concept_name
		, domain_id = dc.domain_id
		, vocabulary_id = dc.vocabulary_id
		, concept_class_id = dc.concept_class_id
		, standard_concept = dc.standard_concept
		, concept_code = dc.concept_code
		, valid_start_date = dc.valid_start_date
		, valid_end_date = dc.valid_end_date
		, invalid_reason = dc.invalid_reason
	FROM vocabulary_updates.concept dc
	WHERE
		cp.concept_id = dc.concept_id
		AND (
        cp.concept_name <> dc.concept_name
        OR cp.domain_id <> dc.domain_id
        OR cp.vocabulary_id <> dc.vocabulary_id
        OR cp.concept_class_id <> dc.concept_class_id
        OR cp.standard_concept <> dc.standard_concept
        OR cp.concept_code <> dc.concept_code
        OR cp.valid_start_date <> dc.valid_start_date
        OR cp.valid_end_date <> dc.valid_end_date
        OR cp.invalid_reason <> dc.invalid_reason
  );

	RAISE NOTICE '% Inserting concept', clock_timestamp();
	INSERT INTO vocabularies.concept(
        concept_id, concept_name
      , domain_id, vocabulary_id
      , concept_class_id, standard_concept
      , concept_code, valid_start_date
      , valid_end_date, invalid_reason
  )
	SELECT
        cp.concept_id, cp.concept_name
      , cp.domain_id, cp.vocabulary_id
      , cp.concept_class_id, cp.standard_concept
      , cp.concept_code, cp.valid_start_date
      , cp.valid_end_date, cp.invalid_reason
	FROM vocabulary_updates.concept cp
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabularies.concept scp
		WHERE scp.concept_id = cp.concept_id
	);

	-- concept_ancestor, concept_relationship and concept_synonym
	-- are not updated, just inserted or deleted. Shall we update
	-- them too?
	RAISE NOTICE '% Deleting concept_ancestor', clock_timestamp();
	DELETE FROM vocabularies.concept_ancestor oca
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabulary_updates.concept_ancestor ca
		WHERE oca.ancestor_concept_id = ca.ancestor_concept_id
			AND oca.descendant_concept_id = ca.descendant_concept_id
	);

	RAISE NOTICE '% Inserting concept_ancestor', clock_timestamp();
	INSERT INTO vocabularies.concept_ancestor(
      ancestor_concept_id, descendant_concept_id
    , min_levels_of_separation, max_levels_of_separation
  )
	SELECT
        ca.ancestor_concept_id, ca.descendant_concept_id
      , ca.min_levels_of_separation, ca.max_levels_of_separation
	FROM vocabulary_updates.concept_ancestor ca
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabularies.concept_ancestor oca
		WHERE oca.ancestor_concept_id = ca.ancestor_concept_id
			AND oca.descendant_concept_id = ca.descendant_concept_id
	);

	RAISE NOTICE '% Deleting concept_relationship', clock_timestamp();
	DELETE FROM vocabularies.concept_relationship ocr
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabulary_updates.concept_relationship cr
		WHERE ocr.concept_id_1 = cr.concept_id_1
			AND ocr.concept_id_2 = cr.concept_id_2
			AND ocr.relationship_id = cr.relationship_id
	);

	RAISE NOTICE '% Inserting concept_relationship', clock_timestamp();
	INSERT INTO vocabularies.concept_relationship(
      concept_id_1, concept_id_2
    , valid_start_date, valid_end_date
    , relationship_id, invalid_reason
  )
	SELECT
      concept_id_1, concept_id_2
    , valid_start_date, valid_end_date
    , relationship_id, invalid_reason
	FROM vocabulary_updates.concept_relationship cr
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabularies.concept_relationship ocr
		WHERE ocr.concept_id_1 = cr.concept_id_1
			AND ocr.concept_id_2 = cr.concept_id_2
			AND ocr.relationship_id = cr.relationship_id
	);

	RAISE NOTICE '% Deleting concept_synonym', clock_timestamp();
	DELETE FROM vocabularies.concept_synonym ocy
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabulary_updates.concept_synonym cy
		WHERE ocy.concept_id = cy.concept_id
			AND ocy.concept_synonym_name = cy.concept_synonym_name
			AND ocy.language_concept_id = cy.language_concept_id
	);

	RAISE NOTICE '% Inserting concept_synonym', clock_timestamp();
	INSERT INTO vocabularies.concept_synonym(concept_id, language_concept_id, concept_synonym_name)
	SELECT
    cy.concept_id, cy.language_concept_id, cy.concept_synonym_name
	FROM vocabulary_updates.concept_synonym cy
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabularies.concept_synonym ocy
		WHERE ocy.concept_id = cy.concept_id
			AND ocy.concept_synonym_name = cy.concept_synonym_name
			AND ocy.language_concept_id = cy.language_concept_id
	);

	RAISE NOTICE '% Deleting drug_strength', clock_timestamp();
	DELETE FROM vocabularies.drug_strength ods
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabulary_updates.drug_strength ds
		WHERE ods.drug_concept_id = ds.drug_concept_id
			AND ods.ingredient_concept_id = ds.ingredient_concept_id
	);

	RAISE NOTICE '% Updating drug_strength', clock_timestamp();
	UPDATE vocabularies.drug_strength ods
	SET
		  amount_value = ds.amount_value
		, amount_unit_concept_id = ds.amount_unit_concept_id
		, numerator_value = ds.numerator_value
		, numerator_unit_concept_id = ds.numerator_unit_concept_id
		, denominator_value = ds.denominator_value
		, denominator_unit_concept_id = ds.denominator_unit_concept_id
		, box_size = ds.box_size
		, valid_start_date = ds.valid_start_date
		, valid_end_date = ds.valid_end_date
		, invalid_reason = ds.invalid_reason
	FROM vocabulary_updates.drug_strength ds
	WHERE ods.drug_concept_id = ds.drug_concept_id
		AND ods.ingredient_concept_id = ds.ingredient_concept_id
		AND (
         ods.amount_value <> ds.amount_value
  		OR ods.amount_unit_concept_id <> ds.amount_unit_concept_id
  		OR ods.numerator_value <> ds.numerator_value
  		OR ods.numerator_unit_concept_id <> ds.numerator_unit_concept_id
  		OR ods.denominator_value <> ds.denominator_value
  		OR ods.denominator_unit_concept_id <> ds.denominator_unit_concept_id
  		OR ods.box_size <> ds.box_size
  		OR ods.valid_start_date <> ds.valid_start_date
  		OR ods.valid_end_date <> ds.valid_end_date
  		OR ods.invalid_reason <> ds.invalid_reason
  );

	RAISE NOTICE '% Inserting drug_strength', clock_timestamp();
	INSERT INTO vocabularies.drug_strength(
      drug_concept_id, ingredient_concept_id
    , valid_start_date, valid_end_date
    , amount_unit_concept_id, numerator_unit_concept_id
    , denominator_unit_concept_id
    , box_size, amount_value
    , numerator_value, denominator_value
    , invalid_reason
  )
	SELECT
      ds.drug_concept_id, ds.ingredient_concept_id
    , ds.valid_start_date, ds.valid_end_date
    , ds.amount_unit_concept_id, ds.numerator_unit_concept_id
    , ds.denominator_unit_concept_id
    , ds.box_size, ds.amount_value
    , ds.numerator_value, ds.denominator_value
    , ds.invalid_reason
	FROM vocabulary_updates.drug_strength ds
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabularies.drug_strength ods
		WHERE ods.drug_concept_id = ds.drug_concept_id
			AND ods.ingredient_concept_id = ds.ingredient_concept_id
	);

	RAISE NOTICE '% Deleting relationship', clock_timestamp();
	DELETE FROM vocabularies.relationship ors
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabulary_updates.relationship rs
		WHERE ors.relationship_id = rs.relationship_id
	);

	RAISE NOTICE '% Deleting concept_class', clock_timestamp();
	DELETE FROM vocabularies.concept_class occ
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabulary_updates.concept_class cc
		WHERE occ.concept_class_id = cc.concept_class_id
	);

	RAISE NOTICE '% Deleting domain', clock_timestamp();
	DELETE FROM vocabularies.domain odm
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabulary_updates.domain dm
		WHERE odm.domain_id = dm.domain_id
	);

	RAISE NOTICE '% Deleting vocabulary', clock_timestamp();
	DELETE FROM vocabularies.vocabulary ovc
	WHERE NOT EXISTS ( SELECT 1
		FROM vocabulary_updates.vocabulary vc
		WHERE ovc.vocabulary_id = vc.vocabulary_id
	)
	AND ovc.vocabulary_id <> 'IOMED';

	RAISE NOTICE '% Updating deprecated concept', clock_timestamp();
	for source_schema, source_table, source_column in
	select sns.nspname, scl.relname, at.attname
	from pg_constraint cn
	join pg_class scl on scl.oid = cn.conrelid
	join pg_namespace sns on sns.oid = scl.relnamespace
	join pg_class ocl on ocl.oid = cn.confrelid
	join pg_namespace ons on ons.oid = ocl.relnamespace
	-- WARNING: FK with multiple columns won't be updated
	join pg_attribute at on at.attrelid = scl.oid and at.attnum=cn.conkey[1]
	where true
		-- FK constraint type
		and contype = 'f'
		-- on update cascade fk
		and cn.confupdtype = 'c'
		-- origin schema and table
		and ons.nspname = 'vocabularies'
		and ocl.relname = 'concept'
		-- First column of origin table, aka concept_id
		-- WARNING: FK with multiple columns won't be updated
		and cn.confkey = '{1}'::smallint[]
		-- Avoid updating vocabularies tables themselves
		and sns.nspname <> 'vocabularies'
		-- Avoid autoreferencing
		-- and scl.oid <> ocl.oid
	loop
		raise notice '% Updating deprecated concepts in %.%(%)', clock_timestamp(), source_schema, source_table, source_column;
		qry := format('
update %I.%I st
set %I = cr.concept_id_1
from vocabulary_updates.concept_relationship cr
where
	cr.concept_id_2 = st.%I
	and cr.relationship_id = %L
', source_schema, source_table, source_column, source_column, 'Concept replaces');
		execute qry;

	end loop;

END
$BODY$;

-- ALTER PROCEDURE vocabularies.update_concepts()
--     OWNER TO bsc_user;

-- GO
