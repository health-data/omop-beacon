//// METADATA name=change0 includeDependencies=public.00_migration.change0
CREATE OR REPLACE PROCEDURE vocabularies.load_update_vocabularies_schema(version text, env text default 'edge')
LANGUAGE 'plpgsql'
AS $BODY$
declare
  base_curl text;
  curl text;
begin

  if env = 'edge' then
    base_curl := 'curl -H "Host: datastorage.hospital.production.iomed.health" https://datastorage.hospital.production.iomed.health:8443/iomed-public-data/OMOP-CDM/';
  else
    base_curl := 'curl https://storage.googleapis.com/iomed-public-data/OMOP-CDM/';
  end if;

  -- load vocabulary table
  curl := base_curl || version || '/VOCABULARY.csv.gz -k -s | zcat';
  execute 'copy vocabularies_update.vocabulary from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  alter table vocabularies_update.vocabulary
    add constraint xpk_vocabulary_id primary key (vocabulary_id);

  -- load concept table
  curl := base_curl || version || '/CONCEPT.csv.gz -k -s | zcat';
  execute 'copy vocabularies_update.concept from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  alter table vocabularies_update.concept
    add constraint xpk_concept_id primary key (concept_id);

  -- load domain table
  curl := base_curl || version || '/DOMAIN.csv.gz -k -s | zcat';
  execute 'copy vocabularies_update.domain from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  alter table vocabularies_update.domain
    add constraint xpk_domain_id primary key (domain_id);

  -- load concept_class table
  curl := base_curl || version || '/CONCEPT_CLASS.csv.gz -k -s | zcat';
  execute 'copy vocabularies_update.concept_class from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  alter table vocabularies_update.concept_class
    add constraint xpk_concept_class_id primary key (concept_class_id);

  -- load relationship table
  curl := base_curl || version || '/RELATIONSHIP.csv.gz -k -s | zcat';
  execute 'copy vocabularies_update.relationship from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  alter table vocabularies_update.relationship
    add constraint xpk_relationship_id primary key (relationship_id);

  -- load concept_relationship table
  curl := base_curl || version || '/CONCEPT_RELATIONSHIP.csv.gz -k -s | zcat';
  execute 'copy vocabularies_update.concept_relationship from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  alter table vocabularies_update.concept_relationship
    add constraint xpk_concept_relationship_id primary key (concept_id_1, concept_id_2, relationship_id);

  -- load concept_synonym table
  curl := base_curl || version || '/CONCEPT_SYNONYM.csv.gz -k -s | zcat';
  execute 'copy vocabularies_update.concept_synonym from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  alter table vocabularies_update.concept_synonym
    add constraint xpk_concept_synonym_id primary key (concept_id, concept_synonym_name, language_concept_id);

  -- load concept_ancestor table
  curl := base_curl || version || '/CONCEPT_ANCESTOR.csv.gz -k -s | zcat';
  execute 'copy vocabularies_update.concept_ancestor from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  alter table vocabularies_update.concept_ancestor
    add constraint xpk_concept_ancestor_id primary key (ancestor_concept_id, descendant_concept_id);

  -- load drug_strength table
  curl := base_curl || version || '/DRUG_STRENGTH.csv.gz -k -s | zcat';
  execute 'copy vocabularies_update.drug_strength from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  alter table vocabularies_update.drug_strength
    add constraint xpk_drug_strength_id primary key (drug_concept_id, ingredient_concept_id);

end;

$BODY$;

-- ALTER PROCEDURE vocabularies.load_update_vocabularies_schema(text, text)
--     OWNER TO bsc_user;

-- GO
