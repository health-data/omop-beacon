//// METADATA name=change0 includeDependencies=public.00_migration.change0
CREATE OR REPLACE PROCEDURE vocabularies.load_fresh_vocabularies(version text, env text default 'edge')
LANGUAGE 'plpgsql'
AS $BODY$
declare
  base_curl text;
  curl text;
begin

  SET CONSTRAINTS ALL DEFERRED;

  base_curl := 'curl https://alabarga:k7bdP-eGqYa-iCNfx-qJi6k-5BKg2@b2drop.bsc.es/remote.php/webdav/OMOP_VOCABULARIES/';

  -- load vocabulary table
  curl := base_curl || version || '/VOCABULARY.csv.gz -k -s | zcat';
  execute 'copy vocabularies.vocabulary (vocabulary_id,vocabulary_name,vocabulary_reference,vocabulary_version,vocabulary_concept_id) from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  -- load concept table
  curl := base_curl || version || '/CONCEPT.csv.gz -k -s | zcat';
  execute 'copy vocabularies.concept (concept_id,concept_name,domain_id,vocabulary_id,concept_class_id,standard_concept,concept_code,valid_start_date,valid_end_date,invalid_reason) from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';


  -- load domain table
  curl := base_curl || version || '/DOMAIN.csv.gz -k -s | zcat';
  execute 'copy vocabularies.domain (domain_id,domain_name,domain_concept_id) from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  -- load concept_class table
  curl := base_curl || version || '/CONCEPT_CLASS.csv.gz -k -s | zcat';
  execute 'copy vocabularies.concept_class (concept_class_id,concept_class_name,concept_class_concept_id) from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  -- load relationship table
  curl := base_curl || version || '/RELATIONSHIP.csv.gz -k -s | zcat';
  execute 'copy vocabularies.relationship (relationship_id,relationship_name,is_hierarchical,defines_ancestry,reverse_relationship_id,relationship_concept_id) from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  -- load concept_relationship table
  curl := base_curl || version || '/CONCEPT_RELATIONSHIP.csv.gz -k -s | zcat';
  execute 'copy vocabularies.concept_relationship (concept_id_1,concept_id_2,relationship_id,valid_start_date,valid_end_date,invalid_reason) from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  -- load concept_synonym table
  curl := base_curl || version || '/CONCEPT_SYNONYM.csv.gz -k -s | zcat';
  execute 'copy vocabularies.concept_synonym (concept_id,concept_synonym_name,language_concept_id) from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  -- load concept_ancestor table
  curl := base_curl || version || '/CONCEPT_ANCESTOR.csv.gz -k -s | zcat';
  execute 'copy vocabularies.concept_ancestor (ancestor_concept_id,descendant_concept_id,min_levels_of_separation,max_levels_of_separation) from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

  -- load drug_strength table
  curl := base_curl || version || '/DRUG_STRENGTH.csv.gz -k -s | zcat';
  execute 'copy vocabularies.drug_strength (drug_concept_id,ingredient_concept_id,amount_value,amount_unit_concept_id,numerator_value,numerator_unit_concept_id,denominator_value,denominator_unit_concept_id,box_size,valid_start_date,valid_end_date,invalid_reason) from program ''' || curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';';

end;

$BODY$;
