-- PROCEDURE: vocabularies.load_vocabularies(text, text)

-- DROP PROCEDURE IF EXISTS vocabularies.load_vocabularies(text, text);

CREATE OR REPLACE PROCEDURE vocabularies.load_vocabularies(
	version text,
	env text DEFAULT 'edge'::text)
LANGUAGE 'plpgsql'
AS $BODY$
declare
  _csvs public.hstore := '
      VOCABULARY=>vocabulary
    , CUSTOM_VOCABULARY=>vocabulary
    , CONCEPT=>concept
    , CUSTOM_CONCEPT=>concept
    , DOMAIN=>domain
    , CONCEPT_CLASS=>concept_class
    , RELATIONSHIP=>relationship
    , CONCEPT_RELATIONSHIP=>concept_relationship
    , CONCEPT_SYNONYM=>concept_synonym
    , CONCEPT_ANCESTOR=>concept_ancestor
    , DRUG_STRENGTH=>drug_strength
';
  _file text;
  _table text;
  _curl text;
  _base_curl text;
begin

    if env = 'edge' then
      _base_curl := 'curl -H "Host: datastorage.hospital.production.iomed.health" https://datastorage.hospital.production.iomed.health:8443/iomed-public-data/OMOP-CDM/';
    else
      _base_curl := 'curl https://storage.googleapis.com/iomed-public-data/OMOP-CDM/';
    end if;

  for _file, _table in
  select * from each(_csvs)
  loop
	 raise notice '%: Loading % CSV', clock_timestamp(), _file;
	 _curl := _base_curl || version || '/' || _file || '.csv.gz -k -s | zcat';
     execute('copy vocabularies_update.' ||_table || ' from program ''' || _curl || ''' with csv header delimiter E''\t'' QUOTE E''\r'';');
  end loop;

	alter table vocabularies_update.vocabulary
	add constraint xpk_vocabulary_id primary key (vocabulary_id);

	alter table vocabularies_update.concept
	add constraint xpk_concept_id primary key (concept_id);

	alter table vocabularies_update.domain
	add constraint xpk_domain_id primary key (domain_id);

	alter table vocabularies_update.concept_class
	add constraint xpk_concept_class_id primary key (concept_class_id);

	alter table vocabularies_update.relationship
	add constraint xpk_relationship primary key (relationship_id);

	alter table vocabularies_update.concept_relationship
	add constraint xpk_concept_relationship_id primary key (concept_id_1, concept_id_2, relationship_id);

	alter table vocabularies_update.concept_ancestor
	add constraint xpk_concept_ancestor_id primary key (ancestor_concept_id, descendant_concept_id);

	alter table vocabularies_update.drug_strength
	add constraint xpk_drug_strength_id primary key (drug_concept_id, ingredient_concept_id);

end;
$BODY$;

-- ALTER PROCEDURE vocabularies.load_vocabularies(text, text)
--     OWNER TO bsc_user;
