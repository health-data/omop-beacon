//// METADATA name=change0 includeDependencies=public.00_migration.change0
CREATE OR REPLACE PROCEDURE vocabularies.create_vocabularies_update_schema()
LANGUAGE 'sql'
AS $BODY$

  create schema if not exists vocabularies_update;

  drop table if exists vocabularies_update.vocabulary;
  create unlogged table vocabularies_update.vocabulary
  (
    vocabulary_id         varchar(20)  not null,
    vocabulary_name       varchar(255) not null,
    vocabulary_reference  varchar(255) not null,
    vocabulary_version    varchar(255),
    vocabulary_concept_id integer      not null
    -- constraint xpk_vocabulary_id primary key (vocabulary_id)
  );

  drop table if exists vocabularies_update.concept;
  create unlogged table vocabularies_update.concept
  (
    concept_id       integer not null,
    concept_name     varchar not null,
    domain_id        varchar not null,
    vocabulary_id    varchar not null,
    concept_class_id varchar not null,
    standard_concept varchar,
    concept_code     varchar not null,
    valid_start_date date not null,
    valid_end_date   date not null,
    invalid_reason   varchar
    -- constraint xpk_concept_id primary key (concept_id)
  );

  drop table if exists vocabularies_update.domain;
  create unlogged table vocabularies_update.domain
  (
    domain_id         varchar(20)  not null,
    domain_name       varchar(255) not null,
    domain_concept_id integer      not null
    -- constraint xpk_domain_id primary key (domain_id)
  );

  drop table if exists vocabularies_update.concept_class;
  create unlogged table vocabularies_update.concept_class
  (
    concept_class_id         varchar(20)  not null,
    concept_class_name       varchar(255) not null,
    concept_class_concept_id integer      not null
    -- constraint xpk_concept_class_id primary key (concept_class_id)
  );

  drop table if exists vocabularies_update.relationship;
  create unlogged table vocabularies_update.relationship
  (
    relationship_id         varchar(20)  not null,
    relationship_name       varchar(255) not null,
    is_hierarchical         varchar(1)   not null,
    defines_ancestry        varchar(1)   not null,
    reverse_relationship_id varchar(20)  not null,
    relationship_concept_id integer      not null
    -- constraint xpk_relationship primary key (relationship_id)
  );

  drop table if exists vocabularies_update.concept_relationship;
  create unlogged table vocabularies_update.concept_relationship
  (
    concept_id_1     integer     not null,
    concept_id_2     integer     not null,
    relationship_id  varchar(20) not null,
    valid_start_date date        not null,
    valid_end_date   date        not null,
    invalid_reason   varchar(1)
    -- constraint xpk_concept_relationship_id primary key (concept_id_1, concept_id_2, relationship_id)
  );

  drop table if exists vocabularies_update.concept_ancestor;
  create unlogged table vocabularies_update.concept_ancestor
  (
    ancestor_concept_id      integer not null,
    descendant_concept_id    integer not null,
    min_levels_of_separation integer not null,
    max_levels_of_separation integer not null
    -- constraint xpk_concept_ancestor_id primary key (ancestor_concept_id, descendant_concept_id)
  );

  drop table if exists vocabularies_update.concept_synonym;
  create unlogged table vocabularies_update.concept_synonym
  (
    concept_id           integer not null,
    concept_synonym_name varchar not null,
    language_concept_id  integer not null
  );

  drop table if exists vocabularies_update.drug_strength;
  create unlogged table vocabularies_update.drug_strength
  (
    drug_concept_id             integer not null,
    ingredient_concept_id       integer not null,
    amount_value                numeric,
    amount_unit_concept_id      integer,
    numerator_value             numeric,
    numerator_unit_concept_id   integer,
    denominator_value           numeric,
    denominator_unit_concept_id integer,
    box_size                    integer,
    valid_start_date            date not null,
    valid_end_date              date not null,
    invalid_reason              varchar(1)
    -- constraint xpk_drug_strength_id primary key (drug_concept_id, ingredient_concept_id)
  );

$BODY$;

-- ALTER PROCEDURE vocabularies.create_vocabularies_update_schema()
--     OWNER TO bsc_user;

-- GO
