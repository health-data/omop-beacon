CREATE OR REPLACE PROCEDURE vocabularies.run_update_concepts(version text)
LANGUAGE 'plpgsql'
AS $BODY$
declare
begin
--  if not iomed.check_version(version) then
    call vocabularies.create_vocabularies_update_schema();
    call vocabularies.load_vocabularies(version);
    commit;

    call vocabularies.update_concepts();
    commit;
		-- execute(format('update iomed.cdm_source set vocabulary_version = %L', iomed.omop_version()));
    drop schema vocabularies_update cascade;
--  end if;
end;
$BODY$;

-- ALTER PROCEDURE vocabularies.run_update_concepts(text)
--     OWNER TO bsc_user;

-- GO
