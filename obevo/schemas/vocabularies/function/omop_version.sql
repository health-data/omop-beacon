CREATE OR REPLACE FUNCTION vocabularies.omop_version()
RETURNS text
LANGUAGE 'sql'
STABLE PARALLEL UNSAFE
AS $BODY$
  select vocabulary_version
  from vocabularies.vocabulary
  where vocabulary_id = 'None'
$BODY$;

-- ALTER FUNCTION vocabularies.omop_version()
--     OWNER TO bsc_user;

-- GO
