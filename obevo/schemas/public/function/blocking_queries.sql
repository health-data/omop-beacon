
CREATE OR REPLACE FUNCTION public.blocking_queries()
RETURNS TABLE(
    blocked_id integer
    , blocked_usename name
    , blocked_query text
    , pid integer
    , usename name
    , application_name text
    , backed_start timestamptz
    , xact_start timestamptz
    , query_start timestamptz
    , state_change timestamptz
    , wait_event_type text
    , wait_event text
    , state text
    , query text
)
LANGUAGE 'plpgsql'
VOLATILE SECURITY DEFINER
AS $$
BEGIN
    RETURN QUERY WITH qry AS (
        SELECT
            s.pid,
            u.rolname,
            s.application_name,
            s.backend_start,
            s.xact_start,
            s.query_start,
            s.state_change,
            s.wait_event_type,
            s.wait_event,
            s.state,
            s.query
        FROM (pg_stat_get_activity(NULL::integer) s(datid, pid, usesysid, application_name, state, query, wait_event_type, wait_event, xact_start, query_start, backend_start, state_change, client_addr, client_hostname, client_port, backend_xid, backend_xmin, backend_type, ssl, sslversion, sslcipher, sslbits, sslcompression, ssl_client_dn, ssl_client_serial, ssl_issuer_dn, gss_auth, gss_princ, gss_enc, leader_pid)
        LEFT JOIN pg_authid u ON ((s.usesysid = u.oid)))
    )
    SELECT
        a.pid blocked_pid,
        a.rolname blocked_usename,
        a.query blocked_query,
        b.pid,
        b.rolname,
        b.application_name,
        b.backend_start,
        b.xact_start,
        b.query_start,
        b.state_change,
        b.wait_event_type,
        b.wait_event,
        b.state,
        b.query
    FROM qry a
    JOIN qry b ON b.pid = ANY (pg_blocking_pids(a.pid))
    ;

END $$;

-- ALTER FUNCTION public.blocking_queries owner TO postgres;
-- GRANT EXECUTE ON FUNCTION public.blocking_queries TO bsc_user;
