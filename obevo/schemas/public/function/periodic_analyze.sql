CREATE OR REPLACE PROCEDURE public.periodic_analyze(timeout interval default '1 hour')
LANGUAGE 'plpgsql'
AS $BODY$
declare
	sch text;
	tbl text;
	max_time timestamptz;
begin

	max_time := clock_timestamp() + timeout;

	for sch, tbl in
	select ts.schemaname, ts.relname
	from pg_stat_all_tables ts
	join pg_class cl on cl.oid = ts.relid
	where
		-- Get local non-partitioned tables and materialized views only
		cl.relkind in ('r', 'm')
		and ts.schemaname not in ('information_schema', 'pg_catalog', 'pg_toast')
		-- Get never analyzed tables, which occurs when K8s kills the server
		and coalesce(ts.last_autoanalyze, ts.last_analyze) is null
	loop
		raise notice '% Analyzing %.%', clock_timestamp(), sch, tbl;
		execute format('analyze %I.%I', sch, tbl);
		if clock_timestamp() > max_time
		then
			raise notice '% exiting due to timeout', clock_timestamp();
			exit;
		end if;
	end loop;

end
$BODY$;

ALTER PROCEDURE public.periodic_analyze(interval)
    OWNER TO postgres;
