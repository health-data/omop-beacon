
create or replace procedure public.create_role(new_role text)
language 'plpgsql' as $$
declare
begin

    if not exists (select 1 from pg_roles where rolname = new_role)
    then
        execute format('create role %I', new_role);
    end if;

end $$;
