//// CHANGE name=change0
CREATE EXTENSION IF NOT EXISTS hstore;
CREATE EXTENSION IF NOT EXISTS pg_trgm;
create extension if not exists pgstattuple;

-- CREATE EXTENSION IF NOT EXISTS cstore_fdw;
-- CREATE EXTENSION IF NOT EXISTS pg_background;
GO

//// CHANGE name=change1

CREATE SERVER IF NOT EXISTS cstore_server FOREIGN DATA WRAPPER cstore_fdw;

GO

//// CHANGE name=change2 dependencies=create_role
CREATE SCHEMA IF NOT EXISTS cdm;
CREATE SCHEMA IF NOT EXISTS vocabularies;

GO

//// CHANGE name=change3 dependencies=create_role
call public.create_role('bsc_user');

-- call public.create_role('external');
-- call public.create_role('querybook');
-- call public.create_role('grafana');
-- call public.create_role('atlas');
-- call public.create_role('dbt');


-- GRANT ALL ON DATABASE iomed TO bsc_user;
-- GRANT ALL ON DATABASE iomed TO dbt;


-- ALTER SCHEMA nlp OWNER TO bsc_user;
-- GRANT USAGE ON SCHEMA nlp TO hosp_manager;
-- GRANT USAGE ON SCHEMA nlp TO querybook;
-- GRANT USAGE ON SCHEMA nlp TO grafana;
-- GRANT USAGE ON SCHEMA nlp TO atlas;
-- GRANT USAGE ON SCHEMA nlp TO dbt;

-- CREATE SCHEMA IF NOT EXISTS cdm AUTHORIZATION bsc_user;
-- ALTER SCHEMA cdm OWNER TO bsc_user;
-- GRANT USAGE ON SCHEMA cdm TO hosp_manager;
-- GRANT USAGE ON SCHEMA cdm TO querybook;
-- GRANT USAGE ON SCHEMA cdm TO grafana;
-- GRANT USAGE ON SCHEMA cdm TO atlas;
-- GRANT USAGE ON SCHEMA cdm TO dbt;

-- CREATE SCHEMA IF NOT EXISTS vocabularies AUTHORIZATION bsc_user;
-- ALTER SCHEMA vocabularies OWNER TO bsc_user;
-- GRANT USAGE ON SCHEMA vocabularies TO hosp_manager;
-- GRANT USAGE ON SCHEMA vocabularies TO querybook;
-- GRANT USAGE ON SCHEMA vocabularies TO grafana;
-- GRANT USAGE ON SCHEMA vocabularies TO atlas;
-- GRANT USAGE ON SCHEMA vocabularies TO dbt;

-- CREATE SCHEMA IF NOT EXISTS compass AUTHORIZATION bsc_user;
-- ALTER SCHEMA compass OWNER TO bsc_user;
-- GRANT USAGE ON SCHEMA compass TO hosp_manager;
-- GRANT USAGE ON SCHEMA compass TO querybook;
-- GRANT USAGE ON SCHEMA compass TO grafana;
-- GRANT USAGE ON SCHEMA compass TO atlas;
-- GRANT USAGE ON SCHEMA compass TO dbt;

GO


