#!/bin/bash

# /var/lib/obevo/resources:/var/lib/obevo/classes:/var/lib/obevo/libs/*
java -cp vendor/obevo-cli-8.1.1-dist/lib/* \
 "com.gs.obevo.dist.Main" deploy \
 -forceEnvSetup \
 -noPrompt \
 -sourcePath schemas \
 -env "$ENVIRONMENT" \
 -deployUserId "$PGUSER" -password "$PGPASSWORD"
