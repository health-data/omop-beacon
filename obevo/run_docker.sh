docker run --rm -it -v ./schemas:/schemas/ -v /tmp/:/tmp/ shantstepanian/obevo deploy \
 -sourcePath /schemas/ \
 -forceEnvSetup \
 -noPrompt \
 -env "$ENVIRONMENT" \
 -deployUserId "$PGUSER" -password "$PGPASSWORD"

