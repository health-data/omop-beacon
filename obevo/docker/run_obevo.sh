#!/bin/bash

java -cp /var/lib/obevo/resources:/var/lib/obevo/classes:/var/lib/obevo/libs/* \
 "com.gs.obevo.dist.Main" deploy \
 -forceEnvSetup \
 -noPrompt \
 -sourcePath /obevo/schemas \
 -env "$ENVIRONMENT" \
 -deployUserId "$PGUSER" -password "$PGPASSWORD"
