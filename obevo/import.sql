
begin transaction;

SET CONSTRAINTS ALL DEFERRED;

\copy vocabularies.concept (concept_id,concept_name,domain_id,vocabulary_id,concept_class_id,standard_concept,concept_code,valid_start_date,valid_end_date,invalid_reason) from '/home/alabarga/BSC/data/OMOP_VOCABULARIES/23-JAN-23/CONCEPT.csv' with csv header delimiter E'\t' QUOTE E'\r';
\copy vocabularies.concept_ancestor (ancestor_concept_id,descendant_concept_id,min_levels_of_separation,max_levels_of_separation) from '/home/alabarga/BSC/data/OMOP_VOCABULARIES/23-JAN-23/CONCEPT_ANCESTOR.csv' with csv header delimiter E'\t' QUOTE E'\r';
\copy vocabularies.concept_synonym (concept_id,concept_synonym_name,language_concept_id) from '/home/alabarga/BSC/data/OMOP_VOCABULARIES/23-JAN-23/CONCEPT_SYNONYM.csv' with csv header delimiter E'\t' QUOTE E'\r';
\copy vocabularies.concept_class (concept_class_id,concept_class_name,concept_class_concept_id) from '/home/alabarga/BSC/data/OMOP_VOCABULARIES/23-JAN-23/CONCEPT_CLASS.csv' with csv header delimiter E'\t' QUOTE E'\r';
\copy vocabularies.concept_relationship (concept_id_1,concept_id_2,relationship_id,valid_start_date,valid_end_date,invalid_reason) from '/home/alabarga/BSC/data/OMOP_VOCABULARIES/23-JAN-23/CONCEPT_RELATIONSHIP.csv' with csv header delimiter E'\t' QUOTE E'\r';
\copy vocabularies.domain (domain_id,domain_name,domain_concept_id) from '/home/alabarga/BSC/data/OMOP_VOCABULARIES/23-JAN-23/DOMAIN.csv' with csv header delimiter E'\t' QUOTE E'\r';
\copy vocabularies.vocabulary (vocabulary_id,vocabulary_name,vocabulary_reference,vocabulary_version,vocabulary_concept_id) from '/home/alabarga/BSC/data/OMOP_VOCABULARIES/23-JAN-23/VOCABULARY.csv' with csv header delimiter E'\t' QUOTE E'\r';
\copy vocabularies.relationship (relationship_id,relationship_name,is_hierarchical,defines_ancestry,reverse_relationship_id,relationship_concept_id) from '/home/alabarga/BSC/data/OMOP_VOCABULARIES/23-JAN-23/RELATIONSHIP.csv' with csv header delimiter E'\t' QUOTE E'\r';
\copy vocabularies.drug_strength (drug_concept_id,ingredient_concept_id,amount_value,amount_unit_concept_id,numerator_value,numerator_unit_concept_id,denominator_value,denominator_unit_concept_id,box_size,valid_start_date,valid_end_date,invalid_reason) from '/home/alabarga/BSC/data/OMOP_VOCABULARIES/23-JAN-23/DRUG_STRENGTH.csv' with csv header delimiter E'\t' QUOTE E'\r';

commit;



  