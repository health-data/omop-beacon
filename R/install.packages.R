#renv::consent(provided = TRUE)
#renv::init(project='/iomed/')

Sys.setenv(GITHUB_PAT="dd423ad789afd09d68916172fa4272c4ae29c983")

install.packages("dotenv")
install.packages('remotes')

# sudo apt-get install openjdk-11-jdk
# sudo R CMD javareconf
# sudo apt-get install -y libpcre2-dev liblzma-dev libbz2-dev libicu-dev libxml2-dev libcurl4-openssl-dev

install.packages('rJava')

remotes::install_github('ohdsi/SqlRender')
remotes::install_github('ohdsi/DatabaseConnector')
remotes::install_github("OHDSI/ETL-Synthea")
remotes::install_github("OHDSI/CommonDataModel", "v5.4")


